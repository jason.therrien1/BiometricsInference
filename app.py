import json
import numpy as np
from scipy.spatial.transform import Rotation as R
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf
import pandas as pd
from datetime import datetime
import time
import argparse
from sqlalchemy import create_engine
import psycopg2

pd.options.mode.chained_assignment = None

my_parser = argparse.ArgumentParser(description='We need to parse the Sub and Topic for MQTT')
my_parser.add_argument('--ip', action = 'store', type=str, required= True)
my_parser.add_argument('--id', action = 'store', type=str, required= True)
my_parser.add_argument('--lstm',action = 'store', type=int, required= True)

args = my_parser.parse_args()
model = tf.keras.models.load_model('1000Epochs')
#print("everything is imported, waiting on trigger")
# dynamodb=boto3.resource('dynamodb')
# table=dynamodb.Table('dev-inferenceRecord')


def lambda_handler(event, context):

    try:
        recievedFile = datetime.utcnow().isoformat()
        data = pd.read_json(event['Records'][0]['dynamodb']['NewImage']['data']['S'])
        cleanedData = cleanData(data)
        output = inference(cleanedData,model)
        epochTime = datetime.fromisoformat(event['Records'][0]['dynamodb']['Keys']['timeStamp']['S']).strftime('%s')
        table.put_item(
            Item = {
                'dateTime': event['Records'][0]['dynamodb']['Keys']['timeStamp']['S'],
                'epochTime': int(epochTime),
                'Stressor': str(output[-1]),
                'Recieved': str(recievedFile),
                'Uploaded': str(datetime.utcnow().isoformat())
            }
        )
        print('Successfully processed {} records.'.format( event['Records'][0]['dynamodb']['Keys']['timeStamp']['S']))
    except Exception as e:
        print('Failed to process {} records. See the error below'.format( event['Records'][0]['dynamodb']['Keys']['timeStamp']['S']))
        print(str(e))
    # message = {"foo": "bar"}
    # client = boto3.client('sns')
    # response = client.publish(
    #     TargetArn ='arn:aws:sns:us-east-1:502371745561:biometrics',
    #     Message =json.dumps({'default': json.dumps(str(output))}) ,
    #     MessageStructure='json'
    # )


def cleanData( dataFrame):
    df_dict = {}
    folders = ['A'] #Dear god this needs refactored
    df_dict['A'] = dataFrame
    window = 0.2
    #2) Converting data to appropriate data types.
    # Convert to datetime format.  We then want to convert the dataframe to chronological order.

    for i in folders:

    # We use errors = 'coerce' to see if 'NaT' appear.  

        df_dict[i]['DateTime'] = pd.to_datetime(df_dict[i]['DateTime'], errors = 'coerce')

        df_dict[i]['DateTime'].isna().value_counts()

    # drop 'NaT' if present.

        df_dict[i] = df_dict[i][pd.notnull(df_dict[i]['DateTime'])].reset_index(drop=True)

    # make sure the rows are listed in chronological order.
        df_dict[i] = df_dict[i].sort_values(by='DateTime').reset_index(drop=True)
    # Convert all boolean values to binary.

    for i in folders:

    # Convert 'CurrentPlayer' 'enemy' to 0 and the player to 1
        df_dict[i]['CurrentPlayer'] = df_dict[i]['CurrentPlayer'].apply(lambda x: 0 if x == 'enemy' else 1)

    # We want to change True/False values to 1 and 0 so we can program when game starts and end.
        df_dict[i]['HasGameBoard'] = df_dict[i]['HasGameBoard'].apply(lambda x: 1 if x == True else 0)
        df_dict[i]['GameOver'] = df_dict[i]['GameOver'].apply(lambda x: 1 if x == True else 0)

        df_dict[i]['LeftEye_IsBlinking'] = df_dict[i]['LeftEye_IsBlinking'].apply(lambda x: 1 if x == True else 0)
        df_dict[i]['RightEye_IsBlinking'] = df_dict[i]['RightEye_IsBlinking'].apply(lambda x: 1 if x == True else 0)

    # Convert 'CalibrationStatus' 'Bad' to 0 and 'Good' to 1
        df_dict[i]['CalibrationStatus'] = df_dict[i]['CalibrationStatus'].apply(lambda x: 0 if x == 'Bad' else 1)
        
        # Make sure 'CurrentCondition' are all integers
        df_dict[i]['CurrentCondition'] = df_dict[i]['CurrentCondition'].astype(int)
    #3) Convert datatime so that it starts at 0.  Keep track of gaps in data and when eye calibration is poor.
    # Create new column with timers that starts at 0 s for each game.  The second timer "smooshes" time gaps.
    # We also keep track of the gap lengths and where they occur.

    for i in folders:
        
        # initialize
        df_dict[i]['Game_Time'] = 0.0
        df_dict[i]['Game_Time_No_Gaps'] = 0.0 
        df_dict[i]['Gap_Lengths'] = None
        
        # Data is gathered at an average interval of 0.02 - 0.03 s.  Any interval larger than this interval is problematic.
        # Initially, let's set the threshold for a gap time at 0.1 s.

        gap_threshold = 0.1


        
        # Directly subtracting the gap will yield two rows with the same time.  We thus want to add an average
        # time gap between entries.

        average_time_gap = 0.025

        for j in range(1):
            
            # create a variable that accounts for the gap time.  This will get bigger as more gaps appear.  
            # The gap time will be subtracted from the later times.

            gap = 0
            
            # indices of the game intervals.
            begin_1 = min(df_dict[i].index)
            end_1 = max(df_dict[i].index)
            
            # starting time is zero.
            df_dict[i]['Game_Time'][begin_1] = 0.0
            df_dict[i]['Game_Time_No_Gaps'][begin_1] = 0.0
            df_dict[i]['Gap_Lengths'][begin_1] = None
            
            # loop over the rows of the game
            for k in range(begin_1+1, end_1 + 1):
                # game time is with respect to the first row.
                df_dict[i]['Game_Time'][k] = (df_dict[i]['DateTime'][k] - df_dict[i]['DateTime'][begin_1]).total_seconds()
                
                # if gaps are greater than the threshold, we record the gap.
                if (df_dict[i]['DateTime'][k] - df_dict[i]['DateTime'][k-1]).total_seconds() > gap_threshold: 
                    gap_length = (df_dict[i]['DateTime'][k] - df_dict[i]['DateTime'][k-1]).total_seconds()
        
                    gap += gap_length - average_time_gap # average_time_gap is added so that there is an average time interval between the entries.
            
                    
        
                else:
                    gap_length = None
                    
                df_dict[i]['Game_Time_No_Gaps'][k] = df_dict[i]['Game_Time'][k] - gap
                
                df_dict[i]['Gap_Lengths'][k] = gap_length
    #JET 10/1/2022- I have no idea what purpose the gap length or 'Game_Time_No_Gaps' columns are playing here, 
    #I feel like at the very least anything they might contribute later can be solved with 2 columns, 'Game_Time' and 'Gap_lengths'
    # where 'Gap_lengths' is just data['Game_Time'].diff()
    #
    # Let's create a new df_games column for the amount of time in each game that had a "Good" calibration status.
    # This will be used for calculating fixations/time for each stressor.

    for i in folders:
        
        # initialize new columns
        df_dict[i]['Game_Time_Good_Cal'] = 0.0
        df_dict[i]['Game_Time_Good_Cal_No_Gaps'] = 0.0

        

        for j in range(1):
            

            # indices of the game intervals.
            begin_1 = min(df_dict[i].index)
            end_1 = max(df_dict[i].index)
            
            # first rows start at zero.
            df_dict[i]['Game_Time_Good_Cal'][begin_1] = 0.0
            df_dict[i]['Game_Time_Good_Cal_No_Gaps'][begin_1] = 0.0
            
            for k in range(begin_1+1, end_1 + 1):
                # the timer pauses if the calibration is bad.
                if df_dict[i]['CalibrationStatus'][k]==0:
                    df_dict[i]['Game_Time_Good_Cal'][k] = df_dict[i]['Game_Time_Good_Cal'][k-1]
                    df_dict[i]['Game_Time_Good_Cal_No_Gaps'][k] = df_dict[i]['Game_Time_Good_Cal_No_Gaps'][k-1]
                # add the time if the calibration is good.
                else:
                    df_dict[i]['Game_Time_Good_Cal'][k] = df_dict[i]['Game_Time_Good_Cal'][k-1] + (df_dict[i]['Game_Time'][k]-df_dict[i]['Game_Time'][k-1])
                    df_dict[i]['Game_Time_Good_Cal_No_Gaps'][k] = df_dict[i]['Game_Time_Good_Cal_No_Gaps'][k-1] + (df_dict[i]['Game_Time_No_Gaps'][k]-df_dict[i]['Game_Time_No_Gaps'][k-1])


    #<!-- 4) Blink Counts -->
    # Blink Count

    for i in folders: 
        
        # initialize new columns
        df_dict[i]['Left_Blink_Count'] = 0
        df_dict[i]['Right_Blink_Count'] = 0

        

        for j in range(1):
            

            # indices of the game intervals.
            begin_1 = min(df_dict[i].index)
            end_1 = max(df_dict[i].index)
        
        
            # Initialize Counters.
            if df_dict[i]['LeftEye_IsBlinking'][begin_1] == 1:
                left_count = 1
            else:
                left_count = 0
            
            if df_dict[i]['RightEye_IsBlinking'][begin_1] == 1:
                right_count = 1
            else:
                right_count = 0
            

        
            for k in range(begin_1+1, end_1 + 1):
                if df_dict[i]['LeftEye_IsBlinking'][k] > df_dict[i]['LeftEye_IsBlinking'][k-1]:
                    left_count += 1
                    df_dict[i]['Left_Blink_Count'][k] = left_count
                
                elif (df_dict[i]['LeftEye_IsBlinking'][k]==1) & (df_dict[i]['LeftEye_IsBlinking'][k-1]==1):
                    df_dict[i]['Left_Blink_Count'][k] = left_count
                else:
                    df_dict[i]['Left_Blink_Count'][k] = 0
                
                if df_dict[i]['RightEye_IsBlinking'][k] > df_dict[i]['RightEye_IsBlinking'][k-1]:
                    right_count += 1
                    df_dict[i]['Right_Blink_Count'][k] = right_count
                
                elif (df_dict[i]['RightEye_IsBlinking'][k]==1) & (df_dict[i]['RightEye_IsBlinking'][k-1]==1):
                    df_dict[i]['Right_Blink_Count'][k] = right_count
                else:
                    df_dict[i]['Right_Blink_Count'][k] = 0

    #<!-- 5) Fixation identification and duration -->
    # We want to pull out components of values in paranthesis.  We must eval these values since they are in paranthesis.
    # Note:  We may want to think about other ways of dealing with NaNs in parantheses.
    # Note:  x,y,z coords for rays lack enough decimal points to make it useful as a unit vector.

    def x_comp(x):
        if x.find('NaN') == 1:
            return 0
        else:
            return eval(x)[0] 

    def y_comp(x):
        if x.find('NaN') == 1:
            return 0
        else:
            return eval(x)[1] 

    def z_comp(x):
        if x.find('NaN') == 1:
            return 0
        else:
            return eval(x)[2] 
    # Fixations

    df_eye_dict = {}

    for n in folders: # use n instead of i because I'm copying this algorithm from a previous file that already contained i loops.

    # Fifth version of calculation of saccades and fixation points.  This one accounts for 'CalibrationStatus'.
    # I also use 'Game_Time_No_Gaps' instead of 'DateTime' since I'm only interested in the difference.  The change speeds up evaluation.

    # We also put a threshold for what is considered motion.  Currently, motion is only triggered if there is motion
    # over at least two time periods.  I assume that if motion only happens over a single time period,
    # it is a recording glitch.

    # According to Wikipedia, saccades last between 20-200 ms.  Some of the values I'm calculating last longer,
    # suggesting they should be categorized as "smooth pursuit movements."  Is it worth distinguishing these types?

    # List includes 'Saccade_Count','Saccade_Distance','Saccade_Duration','Fixation_Count','Fixation_Duration'

        saccade_count = 0
        saccade_distance = 0
        saccade_duration = 0
        fixation_count = 0
        fixation_duration = 0

        distance = 0
        time = 0

        j = 0
        saccades_fixations_3 = []
        

    # Tolerance for what makes a fixation point.
        threshold_time = 0.2
        threshold_distance = 0


        for l in range(1):
            
            # indices of the game intervals.
            begin_1 = min(df_dict[n].index)
            end_1 = max(df_dict[n].index)        
            
            
            # Reset all variables every time a game begins.
            saccade_count = 0
            saccade_distance = 0
            saccade_duration = 0
            fixation_count = 0
            fixation_duration = 0
                
            distance = 0
            time = 0

            
            # start the list with these blank values.
            saccades_fixations_3.append([saccade_count,saccade_distance,saccade_duration,fixation_count,fixation_duration, distance, time])

            
            for i in range(begin_1+1, end_1):
            
            # Note:  Because of the way I'm calculating distance and time, the first entry for games are 0s.    

            # I end the loop one row too soon to avoid running into problems with the second distance calculation.

        
                distance = ((x_comp(df_dict[n]['FixationPoint'][i]) - x_comp(df_dict[n]['FixationPoint'][i-1]))**2 + \
                        (y_comp(df_dict[n]['FixationPoint'][i]) - y_comp(df_dict[n]['FixationPoint'][i-1]))**2 + \
                        (z_comp(df_dict[n]['FixationPoint'][i]) - z_comp(df_dict[n]['FixationPoint'][i-1]))**2)**0.5
            
            # the calculation for the upcoming distance is used to determine if fixation is actually moving.  
            # I assume that a nonzero distance over only one period is a recording glitch.
                distance_2 = ((x_comp(df_dict[n]['FixationPoint'][i+1]) - x_comp(df_dict[n]['FixationPoint'][i]))**2 + \
                        (y_comp(df_dict[n]['FixationPoint'][i+1]) - y_comp(df_dict[n]['FixationPoint'][i]))**2 + \
                        (z_comp(df_dict[n]['FixationPoint'][i+1]) - z_comp(df_dict[n]['FixationPoint'][i]))**2)**0.5
            
                time = (df_dict[n]['Game_Time'][i] - df_dict[n]['Game_Time'][i-1]) # There is a question of gaps.  How do we know the eye wasn't moving within a large gap? Should we skip times with gaps? (I think so.  Later in the if statements, the duration for 'no_gaps' has to be greater than 0.2 to be considered a fixation.)
            
            # CalibrationStatus must be 'Good' to be counted.
                if df_dict[n]['CalibrationStatus'][i] == 1 & df_dict[n]['CalibrationStatus'][i-1] == 1:
                
                # saccade calculator      
                    if distance > threshold_distance:
                
                        if (saccade_distance == 0) & (distance_2 == 0): # motion is considered a recording glitch if it doesn't last more than one row.  We interpret the result as a continuation of a fixation.
                            fixation_duration += time
                    
                            if fixation_count == 0:
                                fixation_count = 1
                            # should add j = i, otherwise the first fixation may be allowed to be less than 0.2 s
                                j = i
                    
                            saccade_distance = 0
                            saccade_duration = 0
                    
                        elif (saccade_distance == 0) & (distance_2 != 0):
                            if ((df_dict[n]['Game_Time_No_Gaps'][i] - df_dict[n]['Game_Time_No_Gaps'][j]) <= threshold_time) & (j > begin_1 ): # check if previously calculated fixation qualifies as a fixation.  If not, we will add the time to the previous saccade.
                                for k in range(j,i):
                                    x = saccades_fixations_3[k] # save these entries as a new variable and use to reset the entries.
                                    if x[0] == 0: # if no previous saccades calculated, this will be the first one.
                                        saccades_fixations_3[k][0] = 1
                                        saccade_count = 1 # start the counter
                                    else:
                                        saccades_fixations_3[k][0] = saccades_fixations_3[k-1][0] # set equal to previous saccade count.
                        
                                # saccade distance
                                    saccades_fixations_3[k][1] = x[5] + saccades_fixations_3[k-1][1]
                                
                                # saccade duration
                                    saccades_fixations_3[k][2] = x[6] + saccades_fixations_3[k-1][2]
                            
                                # fixation count lowers by 1.
                                    saccades_fixations_3[k][3] = x[3] - 1
                            
                                # fixation duration now 0
                                    saccades_fixations_3[k][4] = 0
                            
                            # also need to reset the fixation count
                                fixation_count -= 1
                        
                            # also need to reset the saccade_distance and saccade duration values
                                saccade_distance = saccades_fixations_3[i-1][1]
                                saccade_duration = saccades_fixations_3[i-1][2]
                        
                            else:
                                saccade_count += 1 # only add to counter if the saccade_distance is zero (indicating a new saccade) and when we're not overwriting results.
                        
    
                            fixation_duration = 0 # clear out now that the eye is moving.
                
                            saccade_distance += distance
                            saccade_duration += time  
                    
                        else:
                            fixation_duration = 0 # clear out now that the eye is moving.  Don't actually need at this level of the loop (previously zeroed).         
                            saccade_distance += distance
                            saccade_duration += time  
            
                # fixation duration calculator
                    if distance <= threshold_distance: 
                        if fixation_duration == 0: 
                            j = i # keeps track of index that the fixation point begins
                            fixation_count += 1 # only add to counter if the duration is zero (indicating a new fixation)
                            saccade_distance = 0 # clear now that the eye is fixated
                            saccade_duration = 0 # clear now that the eye is fixated


                        fixation_duration += time
                
                    if ((df_dict[n]['Game_Time_No_Gaps'][i] - df_dict[n]['Game_Time_No_Gaps'][j]) <= threshold_time) & (fixation_duration > 0) & (df_dict[n]['Game_Time'][i] == df_dict[n]['Game_Time'][end_1-1]): #(df_dict[n]['Total_Games'][i+2] > df_dict[n]['Total_Games'][i]) & (j >= begin_1 +1): # check if previously calculated fixation at end of game is long enough to be considered a fixation.  
                        for k in range(j,i):
                            x = saccades_fixations_3[k] # save these entries as a new variable and use to reset the entries.
                            if x[0] == 0: # if no previous saccades calculated, this will be the first one.
                                saccades_fixations_3[k][0] = 1
                                saccade_count = 1 # start the counter
                            else:
                                saccades_fixations_3[k][0] = saccades_fixations_3[k-1][0] # set equal to previous saccade count.
                        
                            # saccade distance
                            saccades_fixations_3[k][1] = x[5] + saccades_fixations_3[k-1][1]
                                
                            # saccade duration
                            saccades_fixations_3[k][2] = x[6] + saccades_fixations_3[k-1][2]
                            
                            # fixation count lowers by 1.
                            saccades_fixations_3[k][3] = x[3] - 1
                            
                            # fixation duration now 0
                            saccades_fixations_3[k][4] = 0
                            
                        # also need to reset the fixation count
                        fixation_count -= 1
                        
                        # also need to reset the saccade_distance and saccade duration values
                        saccade_distance = saccades_fixations_3[i-1][1]
                        saccade_duration = saccades_fixations_3[i-1][2]
                            
                        saccade_count = saccades_fixations_3[i-1][0]
                        fixation_duration = 0
            
                else:
    #            if (df_dict[n]['Game_Time'][i] - df_dict[n]['Game_Time'][j]) <= threshold_time: # check if previously calculated fixation qualifies as a fixation.  If not, we will add the time to the previous saccade.
                    if ((df_dict[n]['Game_Time_No_Gaps'][i] - df_dict[n]['Game_Time_No_Gaps'][j]) <= threshold_time) & (df_dict[n]['CalibrationStatus'][i-1] == 1) & (j > begin_1 ): # check if previously calculated fixation qualifies as a fixation.  If not, we will add the time to the previous saccade.  We also only want to do this at the threshold of a bad calibration region.
    
                        for k in range(j,i):
                            x = saccades_fixations_3[k] # save these entries as a new variable and use to reset the entries.
                            if x[0] == 0: # if no previous saccades calculated, this will be the first one.
                                saccades_fixations_3[k][0] = 1
                                saccade_count = 1 # start the counter
                            else:
                                saccades_fixations_3[k][0] = saccades_fixations_3[k-1][0] # set equal to previous saccade count.
                        
                        # saccade distance
                            saccades_fixations_3[k][1] = x[5] + saccades_fixations_3[k-1][1]
                                
                        # saccade duration
                            saccades_fixations_3[k][2] = x[6] + saccades_fixations_3[k-1][2]
                            
                        # fixation count lowers by 1.
                            saccades_fixations_3[k][3] = x[3] - 1
                            
                        # fixation duration now 0
                            saccades_fixations_3[k][4] = 0
                            
                    # also need to reset the fixation count
                        fixation_count -= 1
                        

                    saccade_distance = 0 # clear now that the data isn't reliable when 'CalibrationStatus' == 0
                    saccade_duration = 0
                    fixation_duration = 0
            
    

            
                saccades_fixations_3.append([saccade_count,saccade_distance,saccade_duration,fixation_count,fixation_duration, distance, time])
        
        # final entry of game are zeros.  
            saccades_fixations_3.append([0,0,0,0,0,0,0])

        
        df_eye_dict[n] = pd.DataFrame(saccades_fixations_3, columns = ['Eye_Motion_Count', 'Motion_Distance', 'Motion_Duration', \
                                                        'Fixation_Count','Fixation_Duration', 'Distance','Duration'])
    # join the dataframes

    for i in folders:

        
        #df_dict[i] = df_dict[i].join(df_eye_dict[i])
        df_dict[i]['Eye_Motion_Count'] = df_eye_dict[i]['Eye_Motion_Count']
        df_dict[i]['Eye_Motion_Distance'] = df_eye_dict[i]['Motion_Distance']
        df_dict[i]['Eye_Motion_Duration'] = df_eye_dict[i]['Motion_Duration']
        df_dict[i]['Fixation_Count'] = df_eye_dict[i]['Fixation_Count']
        df_dict[i]['Fixation_Duration'] = df_eye_dict[i]['Fixation_Duration']
        df_dict[i]['Distance'] = df_eye_dict[i]['Distance']
        df_dict[i]['Duration'] = df_eye_dict[i]['Duration']
        
        
    #<!-- 6) Head Motion analysis -->
    # library for Euler angles

    # unit vectors used to define head.  The x unit vector points away from the player's right ear and the -z unit 
    # vector points away from the player's face.  
    x = [1,0,0]
    neg_z = [0,0,-1]

    # The other unit vectors.
    y = [0,1,0]
    z = [0,0,1]
    # find rotated unit vectors.

    for i in folders:
        
        # initialize new columns.    
        df_dict[i]['x_vector_rotated'] = None
        df_dict[i]['neg_z_vector_rotated'] = None
        
        for j in range(1):
    
            
            # indices of the game intervals.
            begin_1 = min(df_dict[i].index)
            end_1 = max(df_dict[i].index)
            

        
            for k in range(begin_1, end_1 + 1):
                
                # Negative signs placed in front of the rotations about the x- and y-axes.
                head_angle_list=[-x_comp(df_dict[i]['Head_Angle'][k]), -y_comp(df_dict[i]['Head_Angle'][k]), z_comp(df_dict[i]['Head_Angle'][k])]
                
                # lower case values indicate extrinsic rotations about the world coordinates. We want the order to be xyz.
                df_dict[i]['x_vector_rotated'][k] = R.from_euler('xyz', head_angle_list, degrees=True).apply(x)
                df_dict[i]['neg_z_vector_rotated'][k] = R.from_euler('xyz', head_angle_list, degrees=True).apply(neg_z)
    # find cross products and dot products of successive vectors.
    for i in folders:
        
        # initialize new columns.    
        df_dict[i]['x_cross'] = None
        df_dict[i]['neg_z_cross'] = None
        
        df_dict[i]['x_dot'] = None
        df_dict[i]['neg_z_dot'] = None
        
        for j in range(1):

            # indices of the game intervals.
            begin_1 = min(df_dict[i].index)
            end_1 = max(df_dict[i].index)
            

            # Begin at begin_1 + 1 because we're taking the difference between values within the game.
            for k in range(begin_1+1, end_1 + 1):
                
                df_dict[i]['x_cross'][k] = np.cross(df_dict[i]['x_vector_rotated'][k-1], df_dict[i]['x_vector_rotated'][k])
                df_dict[i]['neg_z_cross'][k] = np.cross(df_dict[i]['neg_z_vector_rotated'][k-1], df_dict[i]['neg_z_vector_rotated'][k])
                df_dict[i]['x_dot'][k] = np.dot(df_dict[i]['x_vector_rotated'][k-1], df_dict[i]['x_vector_rotated'][k])
                df_dict[i]['neg_z_dot'][k] = np.dot(df_dict[i]['neg_z_vector_rotated'][k-1], df_dict[i]['neg_z_vector_rotated'][k])
                

    for i in folders:
        df_dict[i]['x_dot'] = df_dict[i]['x_dot'].astype('float')
        df_dict[i]['neg_z_dot'] = df_dict[i]['neg_z_dot'].astype('float')

    # determine type of motion (no motion, head up, down, left, right, cw, ccw)
    # Note this is a CRUDE determination.  We are forcing smooth changes into discrete categories.

    # We also want to calculate the angular displacement, time, and angular speed for each type of motion.

    # max angle the x and neg_z cross products can make for head to be considering turning left or right.  We use the angle
    # between them rather than the y-axis since it makes more sense (?) to measure change in the head with respect to itself.
    threshold_angle = 30

    for i in folders:
        
        # initialize new columns.    
        df_dict[i]['Motion_Type'] = 'no_motion'
        df_dict[i]['Ang_Displacement'] = None
        
        df_dict[i]['Ang_Duration'] = None
        df_dict[i]['Ang_Speed'] = None
        
        
        for j in range(1):
            
            # indices of the game intervals.
            begin_1 = min(df_dict[i].index)
            end_1 = max(df_dict[i].index)
            

            # Begin at begin_1 + 1 because we're taking the difference between values within the game.
            for k in range(begin_1+1, end_1 + 1):
                

        
        # Note the duration is between pairs of rows.  It does NOT account for the total duration of motion types that 
        # occur over multiple rows.
                df_dict[i]['Ang_Duration'][k] = df_dict[i]['Game_Time'][k] - df_dict[i]['Game_Time'][k-1]
        
        # No motion when neither dot product changes.
                if (df_dict[i]['x_dot'][k] >= 1) & (df_dict[i]['neg_z_dot'][k] >= 1):
                
                    df_dict[i]['Motion_Type'][k] = 'no_motion'
                    df_dict[i]['Ang_Displacement'][k] = 0.0
                    
                    
        
        # Determine if head is turning left or right.
                elif np.arccos(np.dot(df_dict[i]['x_cross'][k],df_dict[i]['neg_z_cross'][k])/np.linalg.norm(df_dict[i]['x_cross'][k])/np.linalg.norm(df_dict[i]['neg_z_cross'][k]))*180/np.pi <= threshold_angle:
            
            # The angular displacement can be found by either the arccos of neg_z_dot or x_dot (recall that neg_z and x are unit vectors).
            # They should be similar since they're rotating in (roughly) the same plane.
            # We will use the average.
                    df_dict[i]['Ang_Displacement'][k] = (np.arccos(df_dict[i]['neg_z_dot'][k]) + np.arccos(df_dict[i]['x_dot'][k]))/2
                
            # turning left if dot product between neg_z_cross and the y axis is positive.
                    if np.dot(df_dict[i]['neg_z_cross'][k],y) > 0:
                    
                        df_dict[i]['Motion_Type'][k] = 'left'

                    else:
                        df_dict[i]['Motion_Type'][k] = 'right'
                        

        # Determine if head is tilting up or down.  This occurs if there is less change in the x_vector than the neg_z_vector
        # (meaning the rotation is about x_vector)
                elif np.linalg.norm(df_dict[i]['x_cross'][k]) <= np.linalg.norm(df_dict[i]['neg_z_cross'][k]):
                                    
            # Angular displacement given by arccos of neg_z_dot.
                    df_dict[i]['Ang_Displacement'][k] = np.arccos(df_dict[i]['neg_z_dot'][k])                    
                                    
            # tilting up if dot product between y_cross and x_vector is positive.
                    if np.dot(df_dict[i]['neg_z_cross'][k], df_dict[i]['x_vector_rotated'][k]) > 0:
                    
                        df_dict[i]['Motion_Type'][k] = 'up'
                        
                    else:
                        df_dict[i]['Motion_Type'][k] = 'down'
                        
                
        # The final alternatives are head turning about the neg_z_vector.
                elif np.linalg.norm(df_dict[i]['x_cross'][k]) > np.linalg.norm(df_dict[i]['neg_z_cross'][k]):
                                    
            # Angular displacement given by arccos of x_dot.   
                    df_dict[i]['Ang_Displacement'][k] = np.arccos(df_dict[i]['x_dot'][k]) 
                                    
            # cw (from player's perspective) if dot product between x_cross and neg_z_vector is positive.
                    if np.dot(df_dict[i]['x_cross'][k],df_dict[i]['neg_z_vector_rotated'][k]) > 0:
                    
                        df_dict[i]['Motion_Type'][k] = 'cw'
                    
                    else:
                        df_dict[i]['Motion_Type'][k] = 'ccw'
                        
                        
                        
                df_dict[i]['Ang_Speed'][k] = df_dict[i]['Ang_Displacement'][k] / df_dict[i]['Ang_Duration'][k]
    #<!-- Looking over my head movement results, I realize that the identification often changes row by row.  Since I doubt a head can change its type of movement at the scale of a few hundredths of a second, perhaps I should reconsider how I'm identifying head movement.

    #I propose the following:  a head movement needs to occur over at least two rows for it to be considered motion.  Otherwise, the head is considered stationary. -->

    for i in folders:
        df_dict[i]['Ang_Displacement'] = df_dict[i]['Ang_Displacement'].astype(float)
        df_dict[i]['Ang_Duration'] = df_dict[i]['Ang_Duration'].astype(float)
        df_dict[i]['Ang_Speed'] = df_dict[i]['Ang_Speed'].astype(float)
    # create a copy dataframe to clean the head angle columns
    for i in folders: 
        df = df_dict[i]
    # Group by player and motion type to find the indices of each type of motion
    motion_dic = df.groupby(['Motion_Type']).groups
    # The below loop finds the first and last index for when a motion type is repeated.

    motion_list = []

    # loop runs over motion types.
    for i in range(1):
        for j in df['Motion_Type'].unique():
            
            # Starting index.  Initially, we don't assume repeated motion type, so there are no pairs.
            x = motion_dic[j][0]
            pair = False
            y = 0

            for index in motion_dic[j]:
                # check if the indices are succesive.  Only check if a pair is not currently identified
                if (index - x == 1) & (pair == False):
                    y = index
                    pair = True
                # when a pair is already identified, add to the final value to it.
                elif (index - y == 1) & (pair == True):
                    y = index
                # append the pair when the indices no longer successive.  Reset the starting index
                elif (pair == True) & (index - y !=1):
                    motion_list.append([x,y])
                    x = index
                    pair = False
                else:
                    x = index
        
    # Create new columns for cleaned up head data
    # 'Motion_Type', 'Ang_Displacement', 'Ang_Duration'
    # If motion type is not-successive, assume the head is not moving.

    df['Motion_Type_Clean'] = 'no_motion'
    df['Ang_Displacement_Clean'] = 0
    df['Ang_Duration_Clean'] = 0
    df['Ang_Speed_Clean'] = 0
    df['Ang_Displacement_Clean'] = df['Ang_Displacement_Clean'].astype(float)
    df['Ang_Duration_Clean'] = df['Ang_Duration_Clean'].astype(float)
    df['Ang_Speed_Clean'] = df['Ang_Speed_Clean'].astype(float)
    for i in motion_list:
        ang_displacement = 0
        ang_duration = 0
        ang_speed = 0
        
        for j in range(i[0],i[1]+1):
        
            ang_displacement += df['Ang_Displacement'][j]
            ang_duration += df['Ang_Duration'][j]
            
        ang_speed = ang_displacement/ang_duration
        
        for j in range(i[0],i[1]+1):
            df['Motion_Type_Clean'][j] = df['Motion_Type'][j]
            df['Ang_Displacement_Clean'][j] = ang_displacement
            df['Ang_Duration_Clean'][j] = ang_duration
            df['Ang_Speed_Clean'][j] = ang_speed
            
        
    # Copy columns into the data dictionary.
    for i in folders:

        df_dict[i]['Motion_Type_Clean'] = df['Motion_Type_Clean']
        df_dict[i]['Ang_Displacement_Clean'] = df['Ang_Displacement_Clean']
        df_dict[i]['Ang_Duration_Clean'] = df['Ang_Duration_Clean']
        df_dict[i]['Ang_Speed_Clean']  = df['Ang_Speed_Clean'] 
    #<!-- 7) Fixation quadrant identification -->
    #<!-- This code takes the existing fixations and uses the fixation position to determine the quadrant in which the fixation occurs. The quadrants are upper-right (UR), upper-left (UL), lower-right (LR) and, lower-left(LL). -->
    # function for determining quadrant

    def quadrant(x,y):
        
        if (x >= 0) & (y >= 0):
            return 'UR'
        elif (x < 0) & (y >=0):
            return 'UL'
        elif (x < 0) & (y < 0):
            return 'LL'
        else:
            return 'LR'
    # initialize fixation quadrant column
    df['Fixation_Quadrant'] = None
    # When the fixation count increases, record what quadrant the eye is fixated.
    # Note we don't just want the fixation count to change; the last entry of each game clears the fixation count to zero.

    for i in range(1,len(df['Fixation_Count'])):
        if df['Fixation_Count'][i]> df['Fixation_Count'][i-1]:
            
            x_coord = x_comp(df['FixationPoint'][i])
            y_coord = y_comp(df['FixationPoint'][i])
            
            df['Fixation_Quadrant'][i] = quadrant(x_coord,y_coord)
    # Copy column into the data dictionary.
    for i in folders:

        df_dict[i]['Fixation_Quadrant'] = df['Fixation_Quadrant']
        
        df_dict[i].head(20)
    # Let's find the duration, # of blinks, # of fixations, and average fixation of each turn for the player.

    from math import floor


    features_per_window = []

    # copy dataframe

    for i in folders:
        df = df_dict[i]

    # Fraction of game time that must be calibrated "Good" to consider fixation data.
    time_threshold = 0.9

    for i in range(1):
        
        for j in df['CurrentCondition'].unique():
            
            turn_time_stressor = 0
            turn_time_stressor_no_gaps = 0
            turn_time_stressor_good_cal = 0
            turn_time_stressor_good_cal_no_gaps = 0
            
            for k in range(1):
                
                
                
                # determine if game is longer than the time window.  Since there are gaps in the data, I will use the 'no_gap' times.
                game_time = df[(df['CurrentCondition']==j)]['Game_Time_No_Gaps'].max()

                if floor(game_time/window)!=0:
                    # initial time window
                    begin_time = 0
                    end_time = window
        
                    for l in range(floor(game_time/window)):
        
                    # initial row for beginning and end of time window
                        begin_index = min(df[(df['Game_Time_No_Gaps']>=begin_time) & (df['Game_Time_No_Gaps']<end_time)].index)
                        end_index = max(df[(df['Game_Time_No_Gaps']>=begin_time) & (df['Game_Time_No_Gaps']<end_time)].index)
        
                        df_segment = df[begin_index:end_index]
            
                        # slide time window
                        begin_time += window
                        end_time +=window


                        # timers for individual turns.
                        turn_duration = max(df_segment['Game_Time']) - min(df_segment['Game_Time'])
                        turn_duration_no_gaps = max(df_segment['Game_Time_No_Gaps']) - min(df_segment['Game_Time_No_Gaps'])
                        
                        turn_duration_good_cal = max(df_segment['Game_Time_Good_Cal']) - min(df_segment['Game_Time_Good_Cal'])
                        turn_duration_good_cal_no_gaps = max(df_segment['Game_Time_Good_Cal_No_Gaps']) - min(df_segment['Game_Time_Good_Cal_No_Gaps'])                    
                        
                        # time within the game the middle of turn occurs.
                        turn_time = min(df_segment['Game_Time']) + turn_duration/2 # middle of turn
                        turn_time_no_gaps = min(df_segment['Game_Time_No_Gaps']) + turn_duration_no_gaps/2 # middle of turn

                        turn_time_good_cal = min(df_segment['Game_Time_Good_Cal']) + turn_duration_good_cal/2 # middle of turn
                        turn_time_good_cal_no_gaps = min(df_segment['Game_Time_Good_Cal_No_Gaps']) + turn_duration_good_cal_no_gaps/2 # middle of turn                    
                        
                        # blinks
                        turn_left_blink_count = df_segment[df_segment['LeftEye_IsBlinking'] == 1]['Left_Blink_Count'].nunique() # Only count when 'IsBlinking' == 1 to avoid counting the zero entries.
                        turn_right_blink_count = df_segment[df_segment['RightEye_IsBlinking'] == 1]['Right_Blink_Count'].nunique() # Only count when 'IsBlinking' == 1 to avoid counting the zero entries.
                        
                        # fixation count; average and sd of fixation durations.
                        fixation_durations = []
                        
                        # fixation location
                        fixation_locations = []
                        
                        
                        if df_segment['Fixation_Count'].nunique() <=2:
                            turn_fixation_count = 0
                            fixation_durations.append(0)
                            fixation_locations.append(None)
                        elif df_segment['Fixation_Count'].unique()[-1]==0: # last value is 0 if turn occurs at end of game.
                            if df_segment['Fixation_Count'].nunique()-1 <=2:
                                turn_fixation_count = 0
                                fixation_durations.append(0)
                                fixation_locations.append(None)
                            else:
                                turn_fixation_count = df_segment['Fixation_Count'].nunique()-2 # subtract 2 to avoid counting the starting value (don't want overlap with other turns) and the end 0 value.
                                
                                truncated_list = df_segment['Fixation_Count'].unique().tolist()
                                
                                truncated_list.pop() # remove 0 from the end
                                truncated_list.pop(0) # remove first element
                                
                                for m in truncated_list: # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                    fixation_durations.append(max(df_segment[df_segment['Fixation_Count'] == m]['Fixation_Duration']))
                                    # append the fixation location associated with each fixation.  This occurs at the smallest, non-zero value of fixation duration.
                                    fixation_locations.append(df_segment['Fixation_Quadrant'][df_segment[df_segment['Fixation_Duration'] == min(df_segment[(df_segment['Fixation_Count'] == m) & (df_segment['Fixation_Duration']!= 0)]['Fixation_Duration'])].index[0]])
                                
                        else:
                            turn_fixation_count = df_segment['Fixation_Count'].nunique()-2 # subtract 2 to avoid counting the starting value and the end value (don't want overlap with other turns).
                            for m in range(min(df_segment['Fixation_Count'])+1, max(df_segment['Fixation_Count'])): # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                fixation_durations.append(max(df_segment[df_segment['Fixation_Count'] == m]['Fixation_Duration']))
                                # append the fixation location associated with each fixation.  This occurs at the smallest, non-zero value of fixation duration.
                                fixation_locations.append(df_segment['Fixation_Quadrant'][df_segment[df_segment['Fixation_Duration'] == min(df_segment[(df_segment['Fixation_Count'] == m) & (df_segment['Fixation_Duration']!= 0)]['Fixation_Duration'])].index[0]])
                                
                                
                        turn_fixation_av = np.average(fixation_durations)
                        turn_fixations_sd = np.std(fixation_durations)
                        
                        # initialize time in each quadrant
                        UR_time = 0
                        UL_time = 0
                        LR_time = 0
                        LL_time = 0
                        
                        for p,q in zip(fixation_durations,fixation_locations):
                            if q =='UR':
                                UR_time += p
                            elif q =='UL':
                                UL_time += p
                            elif q == 'LR':
                                LR_time += p
                            elif q == 'LL':
                                LL_time += p
                                
                        # make each time a fraction of fixation duration
                        if sum(fixation_durations)>0:
                            UR_time = UR_time/sum(fixation_durations)
                            UL_time = UL_time/sum(fixation_durations)
                            LR_time = LR_time/sum(fixation_durations)
                            LL_time = LL_time/sum(fixation_durations)
                        
                    
                        
                        # overwrite results if bad calibration during turn.
                        if turn_duration_good_cal_no_gaps < turn_duration_no_gaps*time_threshold: # ignore results if the calibration was bad during turn.
                            turn_fixation_count = None
                            turn_fixation_av = None
                            turn_fixations_sd = None
                            
                            
                            
                            
                            
                        # Saccade count; average and sd of speed, duration, and distance of saccades.
                        saccade_speeds = []
                        saccade_durations = []
                        saccade_distances = []
                        
                        
                        if df_segment['Eye_Motion_Count'].nunique() <=2:
                            turn_saccade_count = 0
                            saccade_speeds.append(0)
                            saccade_durations.append(0)
                            saccade_distances.append(0)
                        elif df_segment['Eye_Motion_Count'].unique()[-1]==0: # last value is 0 if turn occurs at end of game.
                            if df_segment['Eye_Motion_Count'].nunique()-1 <=2:
                                turn_saccade_count = 0
                                saccade_speeds.append(0)
                                saccade_durations.append(0)
                                saccade_distances.append(0)
                            else:
                                turn_saccade_count = df_segment['Eye_Motion_Count'].nunique()-2 # subtract 2 to avoid counting the starting value (don't want overlap with other turns) and the end 0 value.
                                
                                truncated_saccade_list = df_segment['Eye_Motion_Count'].unique().tolist()
                                
                                truncated_saccade_list.pop() # remove 0 from the end
                                truncated_saccade_list.pop(0) # remove first element
                                
                                for m in truncated_saccade_list: # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                    saccade_speeds.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance'])/max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                    saccade_durations.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                    saccade_distances.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance']))
                                
                        else:
                            turn_saccade_count = df_segment['Eye_Motion_Count'].nunique()-2 # subtract 2 to avoid counting the starting value and the end value (don't want overlap with other turns).
                            for m in range(min(df_segment['Eye_Motion_Count'])+1, max(df_segment['Eye_Motion_Count'])): # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                saccade_speeds.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance'])/max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                saccade_durations.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                saccade_distances.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance']))
                                
                        turn_speed_av = np.average(saccade_speeds)
                        turn_speed_sd = np.std(saccade_speeds)
                        
                        turn_duration_av = np.average(saccade_durations)
                        turn_duration_sd = np.std(saccade_durations)
                        
                        turn_distance_av = np.average(saccade_distances)
                        turn_distance_sd = np.std(saccade_distances)
                        
                        # overwrite results if bad calibration during turn.
                        if turn_duration_good_cal_no_gaps < turn_duration_no_gaps*time_threshold: # ignore results if the calibration was bad during turn.
                            turn_saccade_count = None
                            turn_speed_av = None
                            turn_speed_sd = None
                        
                            turn_duration_av = None
                            turn_duration_sd = None
                        
                            turn_distance_av = None
                            turn_distance_sd = None
                        
                        # head ang motion.
                        
                        turn_head_ang_av = np.average(df_segment[df_segment['Ang_Speed'].isna()==False]['Ang_Speed'])
                        
                        # split up average head ang movement into movement around x, y, and z axes.  
                        #x_ang_dis = df_segment[(df_segment['Motion_Type']=='up')|(df_segment['Motion_Type']=='down')]['Ang_Displacement'].sum()/turn_duration
                        #y_ang_dis = df_segment[(df_segment['Motion_Type']=='cw')|(df_segment['Motion_Type']=='ccw')]['Ang_Displacement'].sum()/turn_duration
                        #z_ang_dis = df_segment[(df_segment['Motion_Type']=='left')|(df_segment['Motion_Type']=='right')]['Ang_Displacement'].sum()/turn_duration
                        
                        
                        def ang_average(df):
                            if len(df)>0:
                                return np.average(df)
                            else:
                                return 0
                            
                        def ang_sd(df):
                            if len(df)>0:
                                return np.std(df)
                            else:
                                return 0
                            
                        # Find average head ang movement around x, y, and z axes. This is actually speed.
                        #x_ang_dis = ang_average(df_segment[(df_segment['Motion_Type']=='up')|(df_segment['Motion_Type']=='down')& (df_segment['Ang_Speed'].isna() == False)]['Ang_Speed'])
                        #z_ang_dis = ang_average(df_segment[(df_segment['Motion_Type']=='cw')|(df_segment['Motion_Type']=='ccw')& (df_segment['Ang_Speed'].isna() == False)]['Ang_Speed'])
                        #y_ang_dis = ang_average(df_segment[(df_segment['Motion_Type']=='left')|(df_segment['Motion_Type']=='right')& (df_segment['Ang_Speed'].isna() == False)]['Ang_Speed'])
                        
                        # split further
                        x_ang_speed_up = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='up') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        x_ang_speed_down = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='down') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())        
                        z_ang_speed_cw = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='cw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        z_ang_speed_ccw = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='ccw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())                    
                        y_ang_speed_left = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='left')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        y_ang_speed_right = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='right')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        
                        x_ang_speed_up_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='up') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        x_ang_speed_down_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='down') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())        
                        z_ang_speed_cw_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='cw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        z_ang_speed_ccw_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='ccw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())                    
                        y_ang_speed_left_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='left')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        y_ang_speed_right_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='right')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        
                        
                        # All durations divided by turn duration to get a duration fraction.
                        x_ang_dur_up = df_segment[(df_segment['Motion_Type_Clean']=='up')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        x_ang_dur_down = df_segment[(df_segment['Motion_Type_Clean']=='down')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        z_ang_dur_cw = df_segment[(df_segment['Motion_Type_Clean']=='cw')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        z_ang_dur_ccw = df_segment[(df_segment['Motion_Type_Clean']=='ccw')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        y_ang_dur_left = df_segment[(df_segment['Motion_Type_Clean']=='left')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        y_ang_dur_right = df_segment[(df_segment['Motion_Type_Clean']=='right')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        
                        
                        
                        no_motion_dur = 1-x_ang_dur_up-x_ang_dur_down-z_ang_dur_cw-z_ang_dur_ccw-y_ang_dur_left-y_ang_dur_right
                        
                        # Let's look at the dot products of the head motion.  Let's add the two values for a quantitative sense of head motion.
                        # No change means the dot products are 1.  We will subtract these from the total number of rows.  This value will be 0 if no head motion at all.
                        dot_sum = len(df_segment) - (df_segment['neg_z_dot'].sum() + df_segment['x_dot'].sum())/2
                        
                        # normalize by dividing by turn duration
                        dot_sum = dot_sum / turn_duration
                        
                        # we can also use the change in dot products to get a quantitative sense of the head acceleration
                        diff_array = abs(np.diff(df_segment['x_dot'])/np.diff(df_segment['Game_Time'])) + abs(np.diff(df_segment['neg_z_dot'])/np.diff(df_segment['Game_Time']))
                        
                        head_dot_acc = max(diff_array) - min(diff_array)
                        
                        # head range
                        
                        head_range_z = max(df_segment['neg_z_vector_rotated'].apply(lambda x: x[2])) - min(df_segment['neg_z_vector_rotated'].apply(lambda x: x[2]))
                        head_range_x = max(df_segment['x_vector_rotated'].apply(lambda x: x[0])) - min(df_segment['x_vector_rotated'].apply(lambda x: x[0]))
                        
                        head_range_z = head_range_z / turn_duration
                        head_range_x = head_range_x / turn_duration
                        
                        head_range_up_down = max(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1])) - min(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))
                        head_range_left_right = max(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0])) - min(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))
                        
                        head_range_up_down = head_range_up_down / turn_duration
                        head_range_left_right = head_range_left_right / turn_duration
                        
                        head_speed_up_down = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])) 
                        head_speed_left_right = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])) 
                        
                        head_acc_up_down = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                        head_acc_left_right = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                        
                        head_sd_acc_up_down = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                        head_sd_acc_left_right = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                                            
                        head_sd_speed_up_down = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])) 
                        head_sd_speed_left_right = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])) 
                        
                        
    
                        
                        # head lin motion.
                        #turn_head_speed_av = np.average(df_segment['Head_Speed'])
                        #turn_head_acc_av = np.average(df_segment['Head_Acc'])
                        
                        # split up average head lin movement into movement along x, y, and z axes.  All divided by turn duration to get a displacement "rate".
                        #x_dis = df_segment['Head_x_dis'].sum()/turn_duration
                        #y_dis = df_segment['Head_y_dis'].sum()/turn_duration
                        #z_dis = df_segment['Head_z_dis'].sum()/turn_duration                    
                        
                        # rates
                        turn_left_blink_rate = turn_left_blink_count / turn_duration_no_gaps # no_gaps to avoid the uncertainty about what happens in gaps.
                        turn_right_blink_rate = turn_right_blink_count / turn_duration_no_gaps
                        if turn_duration_good_cal_no_gaps < turn_duration_no_gaps*time_threshold:
                            fixation_rate = None
                        else:
                            fixation_rate = turn_fixation_count / turn_duration_good_cal_no_gaps # good_cal time since we don't count durations in bad calibration regions.
                        
                        # timers for stressor
                        turn_time_stressor_total = turn_time + turn_time_stressor
                        turn_time_stressor_total_no_gaps = turn_time_no_gaps + turn_time_stressor_no_gaps
                        
                        turn_time_stressor_total_good_cal = turn_time_good_cal + turn_time_stressor_good_cal
                        turn_time_stressor_total_good_cal_no_gaps = turn_time_good_cal_no_gaps + turn_time_stressor_good_cal_no_gaps
                    
                        
                        
                        variable_list = [j,l,turn_duration,turn_duration_no_gaps, turn_duration_good_cal,turn_duration_good_cal_no_gaps,\
                                                turn_time,turn_time_no_gaps, turn_time_good_cal, turn_time_good_cal_no_gaps,\
                                            turn_time_stressor_total,turn_time_stressor_total_no_gaps,turn_time_stressor_total_good_cal,turn_time_stressor_total_good_cal_no_gaps,\
                                                turn_left_blink_count,turn_right_blink_count,turn_fixation_count,\
                                            turn_left_blink_rate,turn_right_blink_rate,fixation_rate,\
                                            turn_fixation_av,turn_fixations_sd, \
                                                turn_saccade_count,turn_speed_av,turn_speed_sd,turn_distance_av,turn_distance_sd,turn_duration_av,turn_duration_sd,\
                                                turn_head_ang_av,x_ang_speed_up,x_ang_speed_down,z_ang_speed_cw,z_ang_speed_ccw,\
                                                y_ang_speed_left,y_ang_speed_right,\
                                                x_ang_speed_up_sd,x_ang_speed_down_sd,z_ang_speed_cw_sd,z_ang_speed_ccw_sd,\
                                                y_ang_speed_left_sd,y_ang_speed_right_sd,\
                                                x_ang_dur_up,x_ang_dur_down,z_ang_dur_cw,z_ang_dur_ccw,\
                                                y_ang_dur_left,y_ang_dur_right,no_motion_dur,dot_sum,head_range_z,head_range_x,\
                                                    head_range_up_down,head_range_left_right,\
                                                    head_dot_acc,\
                                        head_speed_up_down,head_speed_left_right,\
                                        head_sd_speed_up_down,head_sd_speed_left_right,\
                                        head_acc_up_down,head_acc_left_right,\
                                        head_sd_acc_up_down,head_sd_acc_left_right, UR_time,LR_time,UL_time,LL_time]
                        

                                
                        features_per_window.append(variable_list)
                                                
                                                #x_ang_dis,y_ang_dis,z_ang_dis,\
                                                #x_ang_dur,y_ang_dur,z_ang_dur,no_motion_dur])
                        
                        

                        
                # for stressor time, add time from previous games.      
                turn_time_stressor += max(df[(df['CurrentCondition']==j)]['Game_Time'])
                turn_time_stressor_no_gaps += max(df[(df['CurrentCondition']==j)]['Game_Time_No_Gaps'])
                turn_time_stressor_good_cal += max(df[(df['CurrentCondition']==j)]['Game_Time_Good_Cal'])
                turn_time_stressor_good_cal_no_gaps += max(df[(df['CurrentCondition']==j)]['Game_Time_Good_Cal_No_Gaps'])
    df_feature_window = pd.DataFrame(features_per_window, columns = ['Stressor','Player_Window_Count',\
                                                                'Turn_Duration','Turn_Duration_No_Gaps','Turn_Duration_Good_Cal','Turn_Duration_Good_Cal_No_Gaps', 'Turn_Time_in_Game (center of turn)',\
                                                                'Turn_Time_No_Gaps','Turn_Time_Good_Cal','Turn_Time_Good_Cal_No_Gaps','Turn_Time_Stressor','Turn_Time_Stressor_No_Gaps','Turn_Time_Stressor_Good_Cal','Turn_Time_Stressor_Good_Cal_No_Gaps','Left_Blinks','Right_Blinks',\
                                                                'Fixations', 'Left_Blink_Rate','Right_Blink_Rate',\
                                                                'Fixation_Rate','Fixation_Duration_Av','Fixation_Duration_SD',\
                                                                'Saccades','Saccades_Speed_Av','Saccades_Speed_SD','Saccades_Distance_Av','Saccades_Distance_SD','Saccades_Duration_Av','Saccades_Duration_SD',\
                                                                'Av_Ang_Speed',\
                                                                'av_speed_up','av_speed_down','av_speed_cw','av_speed_ccw','av_speed_left','av_speed_right',\
                                                                'sd_speed_up','sd_speed_down','sd_speed_cw','sd_speed_ccw','sd_speed_left','sd_speed_right',\
                                                                'dur_frac_up','dur_frac_down','dur_frac_cw','dur_frac_ccw','dur_frac_left','dur_frac_right',\
                                                                'no_motion_dur_frac','dot_sum','head_range_z','head_range_x',\
                                                                'head_range_up_down','head_range_left_right','head_dot_acc',\
                                                                'head_speed_up_down','head_speed_left_right',\
                                                                'head_sd_speed_up_down','head_sd_speed_left_right',\
                                                                'head_acc_up_down','head_acc_left_right',\
                                                                'head_sd_acc_up_down','head_sd_acc_left_right','UR_time','LR_time','UL_time','LL_time'])
                                                                #'x_angle_av_speed','y_angle_av_speed','z_angle_av_speed',\
                                                                #'x_angle_dur_frac','y_angle_dur_frac','z_angle_dur_frac','no_motion_dur_frac'])

    return df_feature_window

def inference(dataFrame,model):
    
    input = dataFrame
    eye_columns = ['Left_Blinks',
                    'Right_Blinks',
                    'Fixations',
                    'Left_Blink_Rate',
                    'Right_Blink_Rate',
                    'Fixation_Rate',
                    'Fixation_Duration_Av',
                    'Fixation_Duration_SD',
                    'Saccades',
                    'Saccades_Speed_Av',
                    'Saccades_Speed_SD',
                    'Saccades_Distance_Av',
                    'Saccades_Distance_SD',
                    'Saccades_Duration_Av',
                    'Saccades_Duration_SD',]

    new_fixation_columns = ['UR_time',
                            'LR_time',
                            'UL_time',
                            'LL_time']

    old_head_columns = ['Av_Ang_Speed',
                        'av_speed_up',
                        'av_speed_down',
                        'av_speed_cw',
                        'av_speed_ccw',
                        'av_speed_left',
                        'av_speed_right',
                        'sd_speed_up',
                        'sd_speed_down',
                        'sd_speed_cw',
                        'sd_speed_ccw',
                        'sd_speed_left',
                        'sd_speed_right',
                        'dur_frac_up',
                        'dur_frac_down',
                        'dur_frac_cw',
                        'dur_frac_ccw',
                        'dur_frac_left',
                        'dur_frac_right',
                        'no_motion_dur_frac']



    new_head_columns = ['head_range_z',
                        'head_range_x',
                        'head_range_up_down',
                        'head_range_left_right',
                        'head_dot_acc',
                        'head_speed_up_down',
                        'head_speed_left_right',
                        'head_sd_speed_up_down',
                        'head_sd_speed_left_right',
                        'head_acc_up_down',
                        'head_acc_left_right',
                        'head_sd_acc_up_down',
                        'head_sd_acc_left_right']

    head_and_eyes_columns = new_head_columns + eye_columns + old_head_columns + new_fixation_columns#eye_columns + new_fixation_columns + old_head_columns + new_head_columns
    input = input[head_and_eyes_columns]
    sc = StandardScaler()
    input = sc.fit_transform(input)
    input = pd.DataFrame(input, columns = head_and_eyes_columns)
    # Create lists for the RNN.
    # Number of turns to consider when making a prediction.
    time_steps = 5

    x_input_data = np.empty([len(input)-time_steps, time_steps, len(input.keys())])
    for i in range(0, len(input)-time_steps):
        x_input_data[i,:,:] = input[i:i+time_steps].to_numpy()

    x_input_data = np.asarray(x_input_data).astype(np.float32)

    rawOutput = model.predict(x_input_data)
    output = []
    for i in range(0,len(rawOutput)):
            output.append(rawOutput[i].argmax())
    return output

def cleanData2(data):
    df_dict = {}
    folders = ['A'] #Dear god this needs refactored
    window = 0.2
    #2) Converting data to appropriate data types.
    # Convert to datetime format.  We then want to convert the dataframe to chronological order.

    for i in folders:

    # We use errors = 'coerce' to see if 'NaT' appear.  

        data['DateTime'] = pd.to_datetime(data['DateTime'], errors = 'coerce')

        data['DateTime'].isna().value_counts()

    # drop 'NaT' if present.

        data = data[pd.notnull(data['DateTime'])].reset_index(drop=True)

    # make sure the rows are listed in chronological order.
        data = data.sort_values(by='DateTime').reset_index(drop=True)
    # Convert all boolean values to binary.

    for i in folders:

    # Convert 'CurrentPlayer' 'enemy' to 0 and the player to 1
        data['CurrentPlayer'] = data['CurrentPlayer'].apply(lambda x: 0 if x == 'enemy' else 1)

    # We want to change True/False values to 1 and 0 so we can program when game starts and end.
        data['HasGameBoard'] = data['HasGameBoard'].apply(lambda x: 1 if x == True else 0)
        data['GameOver'] = data['GameOver'].apply(lambda x: 1 if x == True else 0)

        data['LeftEye_IsBlinking'] = data['LeftEye_IsBlinking'].apply(lambda x: 1 if x == True else 0)
        data['RightEye_IsBlinking'] = data['RightEye_IsBlinking'].apply(lambda x: 1 if x == True else 0)

    # Convert 'CalibrationStatus' 'Bad' to 0 and 'Good' to 1
        data['CalibrationStatus'] = data['CalibrationStatus'].apply(lambda x: 0 if x == 'Bad' else 1)
        
        # Make sure 'CurrentCondition' are all integers
        data['CurrentCondition'] = data['CurrentCondition'].astype(int)
    #3) Convert datatime so that it starts at 0.  Keep track of gaps in data and when eye calibration is poor.
    # Create new column with timers that starts at 0 s for each game.  The second timer "smooshes" time gaps.
    # We also keep track of the gap lengths and where they occur.

        
        # initialize
    data['Game_Time'] = 0.0
    data['Game_Time_No_Gaps'] = 0.0 
    data['Gap_Lengths'] = None
    
    # Data is gathered at an average interval of 0.02 - 0.03 s.  Any interval larger than this interval is problematic.
    # Initially, let's set the threshold for a gap time at 0.1 s.

    gap_threshold = 0.1


    
    # Directly subtracting the gap will yield two rows with the same time.  We thus want to add an average
    # time gap between entries.

    average_time_gap = 0.025
    data['Game_Time'] = (data['DateTime']- pd.Timestamp("1970-01-01"))//pd.Timedelta(1,unit= "microsecond")
    data['Game_Time'] = (data['Game_Time'] - data['Game_Time'][0])*10e-7
    data['Gap_Lengths'] = data['Game_Time'].diff()
    data['Game_Time_No_Gaps'] = data['Gap_Lengths'].apply(lambda x: x-average_time_gap if x > gap_threshold else x).fillna(0)
    data['Game_Time_No_Gaps'] = data['Game_Time_No_Gaps'].cumsum()
    # Let's create a new df_games column for the amount of time in each game that had a "Good" calibration status.
    # This will be used for calculating fixations/time for each stressor.

        
        # initialize new columns
    data['Game_Time_Good_Cal'] = 0.0
    data['Game_Time_Good_Cal_No_Gaps'] = 0.0
    data['temp'] = data[data['CalibrationStatus']==0]['Gap_Lengths']
    data.loc[data['CalibrationStatus']==1,'temp']=0
    data['Game_Time_Good_Cal']= data['Game_Time']-data['temp'].cumsum()
    data['Game_Time_Good_Cal_No_Gaps']= data['Game_Time']-data['temp'].cumsum()
    #<!-- 4) Blink Counts -->
    # Blink Count

    data['temp'] =  data['LeftEye_IsBlinking'].diff()
    #data[data['temp'] == -1 ]=0 ADDS 12 ms to the run time
    data['temp'].apply(lambda x: 1 if x == 1 else 0)# does not effect execution time much
    data['Left_Blink_Count'] = data['temp'].cumsum()
    data['temp'] =  data['RightEye_IsBlinking'].diff()
    #data[data['temp'] == -1 ]=0 ADDS 12 ms to run time (both filters combine to increase run time by 24 ms)
    data['temp'].apply(lambda x: 1 if x == 1 else 0)# does not effect execution time much
    data['Right_Blink_Count'] = data['temp'].cumsum()

    #<!-- 5) Fixation identification and duration -->
    # We want to pull out components of values in paranthesis.  We must eval these values since they are in paranthesis.
    # Note:  We may want to think about other ways of dealing with NaNs in parantheses.
    # Note:  x,y,z coords for rays lack enough decimal points to make it useful as a unit vector.

    def x_comp(x):
        if x.find('NaN') == 1:
            return 0
        else:
            return eval(x)[0] 

    def y_comp(x):
        if x.find('NaN') == 1:
            return 0
        else:
            return eval(x)[1] 

    def z_comp(x):
        if x.find('NaN') == 1:
            return 0
        else:
            return eval(x)[2] 
    # Fixations

    df_eye_dict = {}

    for n in folders: # use n instead of i because I'm copying this algorithm from a previous file that already contained i loops.

    # Fifth version of calculation of saccades and fixation points.  This one accounts for 'CalibrationStatus'.
    # I also use 'Game_Time_No_Gaps' instead of 'DateTime' since I'm only interested in the difference.  The change speeds up evaluation.

    # We also put a threshold for what is considered motion.  Currently, motion is only triggered if there is motion
    # over at least two time periods.  I assume that if motion only happens over a single time period,
    # it is a recording glitch.

    # According to Wikipedia, saccades last between 20-200 ms.  Some of the values I'm calculating last longer,
    # suggesting they should be categorized as "smooth pursuit movements."  Is it worth distinguishing these types?

    # List includes 'Saccade_Count','Saccade_Distance','Saccade_Duration','Fixation_Count','Fixation_Duration'

        saccade_count = 0
        saccade_distance = 0
        saccade_duration = 0
        fixation_count = 0
        fixation_duration = 0

        distance = 0
        time = 0

        j = 0
        saccades_fixations_3 = []
        

    # Tolerance for what makes a fixation point.
        threshold_time = 0.2
        threshold_distance = 0


        for l in range(1):
            
            # indices of the game intervals.
            begin_1 = min(data.index)
            end_1 = max(data.index)        
            
            
            # Reset all variables every time a game begins.
            saccade_count = 0
            saccade_distance = 0
            saccade_duration = 0
            fixation_count = 0
            fixation_duration = 0
                
            distance = 0
            time = 0

            
            # start the list with these blank values.
            saccades_fixations_3.append([saccade_count,saccade_distance,saccade_duration,fixation_count,fixation_duration, distance, time])

            
            for i in range(begin_1+1, end_1):
            
            # Note:  Because of the way I'm calculating distance and time, the first entry for games are 0s.    

            # I end the loop one row too soon to avoid running into problems with the second distance calculation.

        
                distance = ((x_comp(data['FixationPoint'][i]) - x_comp(data['FixationPoint'][i-1]))**2 + \
                        (y_comp(data['FixationPoint'][i]) - y_comp(data['FixationPoint'][i-1]))**2 + \
                        (z_comp(data['FixationPoint'][i]) - z_comp(data['FixationPoint'][i-1]))**2)**0.5
            
            # the calculation for the upcoming distance is used to determine if fixation is actually moving.  
            # I assume that a nonzero distance over only one period is a recording glitch.
                distance_2 = ((x_comp(data['FixationPoint'][i+1]) - x_comp(data['FixationPoint'][i]))**2 + \
                        (y_comp(data['FixationPoint'][i+1]) - y_comp(data['FixationPoint'][i]))**2 + \
                        (z_comp(data['FixationPoint'][i+1]) - z_comp(data['FixationPoint'][i]))**2)**0.5
            
                time = (data['Game_Time'][i] - data['Game_Time'][i-1]) # There is a question of gaps.  How do we know the eye wasn't moving within a large gap? Should we skip times with gaps? (I think so.  Later in the if statements, the duration for 'no_gaps' has to be greater than 0.2 to be considered a fixation.)
            
            # CalibrationStatus must be 'Good' to be counted.
                if data['CalibrationStatus'][i] == 1 & data['CalibrationStatus'][i-1] == 1:
                
                # saccade calculator      
                    if distance > threshold_distance:
                
                        if (saccade_distance == 0) & (distance_2 == 0): # motion is considered a recording glitch if it doesn't last more than one row.  We interpret the result as a continuation of a fixation.
                            fixation_duration += time
                    
                            if fixation_count == 0:
                                fixation_count = 1
                            # should add j = i, otherwise the first fixation may be allowed to be less than 0.2 s
                                j = i
                    
                            saccade_distance = 0
                            saccade_duration = 0
                    
                        elif (saccade_distance == 0) & (distance_2 != 0):
                            if ((data['Game_Time_No_Gaps'][i] - data['Game_Time_No_Gaps'][j]) <= threshold_time) & (j > begin_1 ): # check if previously calculated fixation qualifies as a fixation.  If not, we will add the time to the previous saccade.
                                for k in range(j,i):
                                    x = saccades_fixations_3[k] # save these entries as a new variable and use to reset the entries.
                                    if x[0] == 0: # if no previous saccades calculated, this will be the first one.
                                        saccades_fixations_3[k][0] = 1
                                        saccade_count = 1 # start the counter
                                    else:
                                        saccades_fixations_3[k][0] = saccades_fixations_3[k-1][0] # set equal to previous saccade count.
                        
                                # saccade distance
                                    saccades_fixations_3[k][1] = x[5] + saccades_fixations_3[k-1][1]
                                
                                # saccade duration
                                    saccades_fixations_3[k][2] = x[6] + saccades_fixations_3[k-1][2]
                            
                                # fixation count lowers by 1.
                                    saccades_fixations_3[k][3] = x[3] - 1
                            
                                # fixation duration now 0
                                    saccades_fixations_3[k][4] = 0
                            
                            # also need to reset the fixation count
                                fixation_count -= 1
                        
                            # also need to reset the saccade_distance and saccade duration values
                                saccade_distance = saccades_fixations_3[i-1][1]
                                saccade_duration = saccades_fixations_3[i-1][2]
                        
                            else:
                                saccade_count += 1 # only add to counter if the saccade_distance is zero (indicating a new saccade) and when we're not overwriting results.
                        
    
                            fixation_duration = 0 # clear out now that the eye is moving.
                
                            saccade_distance += distance
                            saccade_duration += time  
                    
                        else:
                            fixation_duration = 0 # clear out now that the eye is moving.  Don't actually need at this level of the loop (previously zeroed).         
                            saccade_distance += distance
                            saccade_duration += time  
            
                # fixation duration calculator
                    if distance <= threshold_distance: 
                        if fixation_duration == 0: 
                            j = i # keeps track of index that the fixation point begins
                            fixation_count += 1 # only add to counter if the duration is zero (indicating a new fixation)
                            saccade_distance = 0 # clear now that the eye is fixated
                            saccade_duration = 0 # clear now that the eye is fixated


                        fixation_duration += time
                
                    if ((data['Game_Time_No_Gaps'][i] - data['Game_Time_No_Gaps'][j]) <= threshold_time) & (fixation_duration > 0) & (data['Game_Time'][i] == data['Game_Time'][end_1-1]): #(data['Total_Games'][i+2] > data['Total_Games'][i]) & (j >= begin_1 +1): # check if previously calculated fixation at end of game is long enough to be considered a fixation.  
                        for k in range(j,i):
                            x = saccades_fixations_3[k] # save these entries as a new variable and use to reset the entries.
                            if x[0] == 0: # if no previous saccades calculated, this will be the first one.
                                saccades_fixations_3[k][0] = 1
                                saccade_count = 1 # start the counter
                            else:
                                saccades_fixations_3[k][0] = saccades_fixations_3[k-1][0] # set equal to previous saccade count.
                        
                            # saccade distance
                            saccades_fixations_3[k][1] = x[5] + saccades_fixations_3[k-1][1]
                                
                            # saccade duration
                            saccades_fixations_3[k][2] = x[6] + saccades_fixations_3[k-1][2]
                            
                            # fixation count lowers by 1.
                            saccades_fixations_3[k][3] = x[3] - 1
                            
                            # fixation duration now 0
                            saccades_fixations_3[k][4] = 0
                            
                        # also need to reset the fixation count
                        fixation_count -= 1
                        
                        # also need to reset the saccade_distance and saccade duration values
                        saccade_distance = saccades_fixations_3[i-1][1]
                        saccade_duration = saccades_fixations_3[i-1][2]
                            
                        saccade_count = saccades_fixations_3[i-1][0]
                        fixation_duration = 0
            
                else:
    #            if (data['Game_Time'][i] - data['Game_Time'][j]) <= threshold_time: # check if previously calculated fixation qualifies as a fixation.  If not, we will add the time to the previous saccade.
                    if ((data['Game_Time_No_Gaps'][i] - data['Game_Time_No_Gaps'][j]) <= threshold_time) & (data['CalibrationStatus'][i-1] == 1) & (j > begin_1 ): # check if previously calculated fixation qualifies as a fixation.  If not, we will add the time to the previous saccade.  We also only want to do this at the threshold of a bad calibration region.
    
                        for k in range(j,i):
                            x = saccades_fixations_3[k] # save these entries as a new variable and use to reset the entries.
                            if x[0] == 0: # if no previous saccades calculated, this will be the first one.
                                saccades_fixations_3[k][0] = 1
                                saccade_count = 1 # start the counter
                            else:
                                saccades_fixations_3[k][0] = saccades_fixations_3[k-1][0] # set equal to previous saccade count.
                        
                        # saccade distance
                            saccades_fixations_3[k][1] = x[5] + saccades_fixations_3[k-1][1]
                                
                        # saccade duration
                            saccades_fixations_3[k][2] = x[6] + saccades_fixations_3[k-1][2]
                            
                        # fixation count lowers by 1.
                            saccades_fixations_3[k][3] = x[3] - 1
                            
                        # fixation duration now 0
                            saccades_fixations_3[k][4] = 0
                            
                    # also need to reset the fixation count
                        fixation_count -= 1
                        

                    saccade_distance = 0 # clear now that the data isn't reliable when 'CalibrationStatus' == 0
                    saccade_duration = 0
                    fixation_duration = 0
            
    

            
                saccades_fixations_3.append([saccade_count,saccade_distance,saccade_duration,fixation_count,fixation_duration, distance, time])
        
        # final entry of game are zeros.  
            saccades_fixations_3.append([0,0,0,0,0,0,0])

        
        df_eye_dict[n] = pd.DataFrame(saccades_fixations_3, columns = ['Eye_Motion_Count', 'Motion_Distance', 'Motion_Duration', \
                                                        'Fixation_Count','Fixation_Duration', 'Distance','Duration'])
    # join the dataframes

    for i in folders:

        
        #data = data.join(df_eye_dict[i])
        data['Eye_Motion_Count'] = df_eye_dict[i]['Eye_Motion_Count']
        data['Eye_Motion_Distance'] = df_eye_dict[i]['Motion_Distance']
        data['Eye_Motion_Duration'] = df_eye_dict[i]['Motion_Duration']
        data['Fixation_Count'] = df_eye_dict[i]['Fixation_Count']
        data['Fixation_Duration'] = df_eye_dict[i]['Fixation_Duration']
        data['Distance'] = df_eye_dict[i]['Distance']
        data['Duration'] = df_eye_dict[i]['Duration']
        
        
    #<!-- 6) Head Motion analysis -->
    # library for Euler angles

    # unit vectors used to define head.  The x unit vector points away from the player's right ear and the -z unit 
    # vector points away from the player's face.  
    x = [1,0,0]
    neg_z = [0,0,-1]

    # The other unit vectors.
    y = [0,1,0]
    z = [0,0,1]
    # find rotated unit vectors.

    for i in folders:
        
        # initialize new columns.    
        data['x_vector_rotated'] = None
        data['neg_z_vector_rotated'] = None
        
        for j in range(1):
    
            
            # indices of the game intervals.
            begin_1 = min(data.index)
            end_1 = max(data.index)
            

        
            for k in range(begin_1, end_1 + 1):
                
                # Negative signs placed in front of the rotations about the x- and y-axes.
                head_angle_list=[-x_comp(data['Head_Angle'][k]), -y_comp(data['Head_Angle'][k]), z_comp(data['Head_Angle'][k])]
                
                # lower case values indicate extrinsic rotations about the world coordinates. We want the order to be xyz.
                data['x_vector_rotated'][k] = R.from_euler('xyz', head_angle_list, degrees=True).apply(x)
                data['neg_z_vector_rotated'][k] = R.from_euler('xyz', head_angle_list, degrees=True).apply(neg_z)
    # find cross products and dot products of successive vectors.
    for i in folders:
        
        # initialize new columns.    
        data['x_cross'] = None
        data['neg_z_cross'] = None
        
        data['x_dot'] = None
        data['neg_z_dot'] = None
        
        for j in range(1):

            # indices of the game intervals.
            begin_1 = min(data.index)
            end_1 = max(data.index)
            

            # Begin at begin_1 + 1 because we're taking the difference between values within the game.
            for k in range(begin_1+1, end_1 + 1):
                
                data['x_cross'][k] = np.cross(data['x_vector_rotated'][k-1], data['x_vector_rotated'][k])
                data['neg_z_cross'][k] = np.cross(data['neg_z_vector_rotated'][k-1], data['neg_z_vector_rotated'][k])
                data['x_dot'][k] = np.dot(data['x_vector_rotated'][k-1], data['x_vector_rotated'][k])
                data['neg_z_dot'][k] = np.dot(data['neg_z_vector_rotated'][k-1], data['neg_z_vector_rotated'][k])
                

    for i in folders:
        data['x_dot'] = data['x_dot'].astype('float')
        data['neg_z_dot'] = data['neg_z_dot'].astype('float')

    # determine type of motion (no motion, head up, down, left, right, cw, ccw)
    # Note this is a CRUDE determination.  We are forcing smooth changes into discrete categories.

    # We also want to calculate the angular displacement, time, and angular speed for each type of motion.

    # max angle the x and neg_z cross products can make for head to be considering turning left or right.  We use the angle
    # between them rather than the y-axis since it makes more sense (?) to measure change in the head with respect to itself.
    threshold_angle = 30

    for i in folders:
        
        # initialize new columns.    
        data['Motion_Type'] = 'no_motion'
        data['Ang_Displacement'] = None
        
        data['Ang_Duration'] = None
        data['Ang_Speed'] = None
        
        
        for j in range(1):
            
            # indices of the game intervals.
            begin_1 = min(data.index)
            end_1 = max(data.index)
            

            # Begin at begin_1 + 1 because we're taking the difference between values within the game.
            for k in range(begin_1+1, end_1 + 1):
                

        
        # Note the duration is between pairs of rows.  It does NOT account for the total duration of motion types that 
        # occur over multiple rows.
                data['Ang_Duration'][k] = data['Game_Time'][k] - data['Game_Time'][k-1]
        
        # No motion when neither dot product changes.
                if (data['x_dot'][k] >= 1) & (data['neg_z_dot'][k] >= 1):
                
                    data['Motion_Type'][k] = 'no_motion'
                    data['Ang_Displacement'][k] = 0.0
                    
                    
        
        # Determine if head is turning left or right.
                elif np.arccos(np.dot(data['x_cross'][k],data['neg_z_cross'][k])/np.linalg.norm(data['x_cross'][k])/np.linalg.norm(data['neg_z_cross'][k]))*180/np.pi <= threshold_angle:
            
            # The angular displacement can be found by either the arccos of neg_z_dot or x_dot (recall that neg_z and x are unit vectors).
            # They should be similar since they're rotating in (roughly) the same plane.
            # We will use the average.
                    data['Ang_Displacement'][k] = (np.arccos(data['neg_z_dot'][k]) + np.arccos(data['x_dot'][k]))/2
                
            # turning left if dot product between neg_z_cross and the y axis is positive.
                    if np.dot(data['neg_z_cross'][k],y) > 0:
                    
                        data['Motion_Type'][k] = 'left'

                    else:
                        data['Motion_Type'][k] = 'right'
                        

        # Determine if head is tilting up or down.  This occurs if there is less change in the x_vector than the neg_z_vector
        # (meaning the rotation is about x_vector)
                elif np.linalg.norm(data['x_cross'][k]) <= np.linalg.norm(data['neg_z_cross'][k]):
                                    
            # Angular displacement given by arccos of neg_z_dot.
                    data['Ang_Displacement'][k] = np.arccos(data['neg_z_dot'][k])                    
                                    
            # tilting up if dot product between y_cross and x_vector is positive.
                    if np.dot(data['neg_z_cross'][k], data['x_vector_rotated'][k]) > 0:
                    
                        data['Motion_Type'][k] = 'up'
                        
                    else:
                        data['Motion_Type'][k] = 'down'
                        
                
        # The final alternatives are head turning about the neg_z_vector.
                elif np.linalg.norm(data['x_cross'][k]) > np.linalg.norm(data['neg_z_cross'][k]):
                                    
            # Angular displacement given by arccos of x_dot.   
                    data['Ang_Displacement'][k] = np.arccos(data['x_dot'][k]) 
                                    
            # cw (from player's perspective) if dot product between x_cross and neg_z_vector is positive.
                    if np.dot(data['x_cross'][k],data['neg_z_vector_rotated'][k]) > 0:
                    
                        data['Motion_Type'][k] = 'cw'
                    
                    else:
                        data['Motion_Type'][k] = 'ccw'
                        
                        
                        
                data['Ang_Speed'][k] = data['Ang_Displacement'][k] / data['Ang_Duration'][k]
    #<!-- Looking over my head movement results, I realize that the identification often changes row by row.  Since I doubt a head can change its type of movement at the scale of a few hundredths of a second, perhaps I should reconsider how I'm identifying head movement.

    #I propose the following:  a head movement needs to occur over at least two rows for it to be considered motion.  Otherwise, the head is considered stationary. -->

    for i in folders:
        data['Ang_Displacement'] = data['Ang_Displacement'].astype(float)
        data['Ang_Duration'] = data['Ang_Duration'].astype(float)
        data['Ang_Speed'] = data['Ang_Speed'].astype(float)
    # create a copy dataframe to clean the head angle columns
    for i in folders: 
        df = data
    # Group by player and motion type to find the indices of each type of motion
    motion_dic = df.groupby(['Motion_Type']).groups
    # The below loop finds the first and last index for when a motion type is repeated.

    motion_list = []

    # loop runs over motion types.
    for i in range(1):
        for j in df['Motion_Type'].unique():
            
            # Starting index.  Initially, we don't assume repeated motion type, so there are no pairs.
            x = motion_dic[j][0]
            pair = False
            y = 0

            for index in motion_dic[j]:
                # check if the indices are succesive.  Only check if a pair is not currently identified
                if (index - x == 1) & (pair == False):
                    y = index
                    pair = True
                # when a pair is already identified, add to the final value to it.
                elif (index - y == 1) & (pair == True):
                    y = index
                # append the pair when the indices no longer successive.  Reset the starting index
                elif (pair == True) & (index - y !=1):
                    motion_list.append([x,y])
                    x = index
                    pair = False
                else:
                    x = index
        
    # Create new columns for cleaned up head data
    # 'Motion_Type', 'Ang_Displacement', 'Ang_Duration'
    # If motion type is not-successive, assume the head is not moving.

    df['Motion_Type_Clean'] = 'no_motion'
    df['Ang_Displacement_Clean'] = 0
    df['Ang_Duration_Clean'] = 0
    df['Ang_Speed_Clean'] = 0
    df['Ang_Displacement_Clean'] = df['Ang_Displacement_Clean'].astype(float)
    df['Ang_Duration_Clean'] = df['Ang_Duration_Clean'].astype(float)
    df['Ang_Speed_Clean'] = df['Ang_Speed_Clean'].astype(float)
    for i in motion_list:
        ang_displacement = 0
        ang_duration = 0
        ang_speed = 0
        
        for j in range(i[0],i[1]+1):
        
            ang_displacement += df['Ang_Displacement'][j]
            ang_duration += df['Ang_Duration'][j]
            
        ang_speed = ang_displacement/ang_duration
        
        for j in range(i[0],i[1]+1):
            df['Motion_Type_Clean'][j] = df['Motion_Type'][j]
            df['Ang_Displacement_Clean'][j] = ang_displacement
            df['Ang_Duration_Clean'][j] = ang_duration
            df['Ang_Speed_Clean'][j] = ang_speed
            
        
    # Copy columns into the data dictionary.
    for i in folders:

        data['Motion_Type_Clean'] = df['Motion_Type_Clean']
        data['Ang_Displacement_Clean'] = df['Ang_Displacement_Clean']
        data['Ang_Duration_Clean'] = df['Ang_Duration_Clean']
        data['Ang_Speed_Clean']  = df['Ang_Speed_Clean'] 
    #<!-- 7) Fixation quadrant identification -->
    #<!-- This code takes the existing fixations and uses the fixation position to determine the quadrant in which the fixation occurs. The quadrants are upper-right (UR), upper-left (UL), lower-right (LR) and, lower-left(LL). -->
    # function for determining quadrant

    def quadrant(x,y):
        
        if (x >= 0) & (y >= 0):
            return 'UR'
        elif (x < 0) & (y >=0):
            return 'UL'
        elif (x < 0) & (y < 0):
            return 'LL'
        else:
            return 'LR'
    # initialize fixation quadrant column
    df['Fixation_Quadrant'] = None
    # When the fixation count increases, record what quadrant the eye is fixated.
    # Note we don't just want the fixation count to change; the last entry of each game clears the fixation count to zero.

    for i in range(1,len(df['Fixation_Count'])):
        if df['Fixation_Count'][i]> df['Fixation_Count'][i-1]:
            
            x_coord = x_comp(df['FixationPoint'][i])
            y_coord = y_comp(df['FixationPoint'][i])
            
            df['Fixation_Quadrant'][i] = quadrant(x_coord,y_coord)
    # Copy column into the data dictionary.
    for i in folders:

        data['Fixation_Quadrant'] = df['Fixation_Quadrant']
        
        data.head(20)
    # Let's find the duration, # of blinks, # of fixations, and average fixation of each turn for the player.

    from math import floor


    features_per_window = []

    # copy dataframe

    for i in folders:
        df = data

    # Fraction of game time that must be calibrated "Good" to consider fixation data.
    time_threshold = 0.9

    for i in range(1):
        
        for j in df['CurrentCondition'].unique():
            
            turn_time_stressor = 0
            turn_time_stressor_no_gaps = 0
            turn_time_stressor_good_cal = 0
            turn_time_stressor_good_cal_no_gaps = 0
            
            for k in range(1):
                
                
                
                # determine if game is longer than the time window.  Since there are gaps in the data, I will use the 'no_gap' times.
                game_time = df[(df['CurrentCondition']==j)]['Game_Time_No_Gaps'].max()

                if floor(game_time/window)!=0:
                    # initial time window
                    begin_time = 0
                    end_time = window
        
                    for l in range(floor(game_time/window)):
        
                    # initial row for beginning and end of time window
                        begin_index = min(df[(df['Game_Time_No_Gaps']>=begin_time) & (df['Game_Time_No_Gaps']<end_time)].index)
                        end_index = max(df[(df['Game_Time_No_Gaps']>=begin_time) & (df['Game_Time_No_Gaps']<end_time)].index)
        
                        df_segment = df[begin_index:end_index]
            
                        # slide time window
                        begin_time += window
                        end_time +=window


                        # timers for individual turns.
                        turn_duration = max(df_segment['Game_Time']) - min(df_segment['Game_Time'])
                        turn_duration_no_gaps = max(df_segment['Game_Time_No_Gaps']) - min(df_segment['Game_Time_No_Gaps'])
                        
                        turn_duration_good_cal = max(df_segment['Game_Time_Good_Cal']) - min(df_segment['Game_Time_Good_Cal'])
                        turn_duration_good_cal_no_gaps = max(df_segment['Game_Time_Good_Cal_No_Gaps']) - min(df_segment['Game_Time_Good_Cal_No_Gaps'])                    
                        
                        # time within the game the middle of turn occurs.
                        turn_time = min(df_segment['Game_Time']) + turn_duration/2 # middle of turn
                        turn_time_no_gaps = min(df_segment['Game_Time_No_Gaps']) + turn_duration_no_gaps/2 # middle of turn

                        turn_time_good_cal = min(df_segment['Game_Time_Good_Cal']) + turn_duration_good_cal/2 # middle of turn
                        turn_time_good_cal_no_gaps = min(df_segment['Game_Time_Good_Cal_No_Gaps']) + turn_duration_good_cal_no_gaps/2 # middle of turn                    
                        
                        # blinks
                        turn_left_blink_count = df_segment[df_segment['LeftEye_IsBlinking'] == 1]['Left_Blink_Count'].nunique() # Only count when 'IsBlinking' == 1 to avoid counting the zero entries.
                        turn_right_blink_count = df_segment[df_segment['RightEye_IsBlinking'] == 1]['Right_Blink_Count'].nunique() # Only count when 'IsBlinking' == 1 to avoid counting the zero entries.
                        
                        # fixation count; average and sd of fixation durations.
                        fixation_durations = []
                        
                        # fixation location
                        fixation_locations = []
                        
                        
                        if df_segment['Fixation_Count'].nunique() <=2:
                            turn_fixation_count = 0
                            fixation_durations.append(0)
                            fixation_locations.append(None)
                        elif df_segment['Fixation_Count'].unique()[-1]==0: # last value is 0 if turn occurs at end of game.
                            if df_segment['Fixation_Count'].nunique()-1 <=2:
                                turn_fixation_count = 0
                                fixation_durations.append(0)
                                fixation_locations.append(None)
                            else:
                                turn_fixation_count = df_segment['Fixation_Count'].nunique()-2 # subtract 2 to avoid counting the starting value (don't want overlap with other turns) and the end 0 value.
                                
                                truncated_list = df_segment['Fixation_Count'].unique().tolist()
                                
                                truncated_list.pop() # remove 0 from the end
                                truncated_list.pop(0) # remove first element
                                
                                for m in truncated_list: # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                    fixation_durations.append(max(df_segment[df_segment['Fixation_Count'] == m]['Fixation_Duration']))
                                    # append the fixation location associated with each fixation.  This occurs at the smallest, non-zero value of fixation duration.
                                    fixation_locations.append(df_segment['Fixation_Quadrant'][df_segment[df_segment['Fixation_Duration'] == min(df_segment[(df_segment['Fixation_Count'] == m) & (df_segment['Fixation_Duration']!= 0)]['Fixation_Duration'])].index[0]])
                                
                        else:
                            turn_fixation_count = df_segment['Fixation_Count'].nunique()-2 # subtract 2 to avoid counting the starting value and the end value (don't want overlap with other turns).
                            for m in range(min(df_segment['Fixation_Count'])+1, max(df_segment['Fixation_Count'])): # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                fixation_durations.append(max(df_segment[df_segment['Fixation_Count'] == m]['Fixation_Duration']))
                                # append the fixation location associated with each fixation.  This occurs at the smallest, non-zero value of fixation duration.
                                fixation_locations.append(df_segment['Fixation_Quadrant'][df_segment[df_segment['Fixation_Duration'] == min(df_segment[(df_segment['Fixation_Count'] == m) & (df_segment['Fixation_Duration']!= 0)]['Fixation_Duration'])].index[0]])
                                
                                
                        turn_fixation_av = np.average(fixation_durations)
                        turn_fixations_sd = np.std(fixation_durations)
                        
                        # initialize time in each quadrant
                        UR_time = 0
                        UL_time = 0
                        LR_time = 0
                        LL_time = 0
                        
                        for p,q in zip(fixation_durations,fixation_locations):
                            if q =='UR':
                                UR_time += p
                            elif q =='UL':
                                UL_time += p
                            elif q == 'LR':
                                LR_time += p
                            elif q == 'LL':
                                LL_time += p
                                
                        # make each time a fraction of fixation duration
                        if sum(fixation_durations)>0:
                            UR_time = UR_time/sum(fixation_durations)
                            UL_time = UL_time/sum(fixation_durations)
                            LR_time = LR_time/sum(fixation_durations)
                            LL_time = LL_time/sum(fixation_durations)
                        
                    
                        
                        # overwrite results if bad calibration during turn.
                        if turn_duration_good_cal_no_gaps < turn_duration_no_gaps*time_threshold: # ignore results if the calibration was bad during turn.
                            turn_fixation_count = None
                            turn_fixation_av = None
                            turn_fixations_sd = None
                            
                            
                            
                            
                            
                        # Saccade count; average and sd of speed, duration, and distance of saccades.
                        saccade_speeds = []
                        saccade_durations = []
                        saccade_distances = []
                        
                        
                        if df_segment['Eye_Motion_Count'].nunique() <=2:
                            turn_saccade_count = 0
                            saccade_speeds.append(0)
                            saccade_durations.append(0)
                            saccade_distances.append(0)
                        elif df_segment['Eye_Motion_Count'].unique()[-1]==0: # last value is 0 if turn occurs at end of game.
                            if df_segment['Eye_Motion_Count'].nunique()-1 <=2:
                                turn_saccade_count = 0
                                saccade_speeds.append(0)
                                saccade_durations.append(0)
                                saccade_distances.append(0)
                            else:
                                turn_saccade_count = df_segment['Eye_Motion_Count'].nunique()-2 # subtract 2 to avoid counting the starting value (don't want overlap with other turns) and the end 0 value.
                                
                                truncated_saccade_list = df_segment['Eye_Motion_Count'].unique().tolist()
                                
                                truncated_saccade_list.pop() # remove 0 from the end
                                truncated_saccade_list.pop(0) # remove first element
                                
                                for m in truncated_saccade_list: # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                    saccade_speeds.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance'])/max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                    saccade_durations.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                    saccade_distances.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance']))
                                
                        else:
                            turn_saccade_count = df_segment['Eye_Motion_Count'].nunique()-2 # subtract 2 to avoid counting the starting value and the end value (don't want overlap with other turns).
                            for m in range(min(df_segment['Eye_Motion_Count'])+1, max(df_segment['Eye_Motion_Count'])): # ignore first and last fixations when finding the average to avoid overlapping of values in turns.
                                saccade_speeds.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance'])/max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                saccade_durations.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Duration']))
                                saccade_distances.append(max(df_segment[df_segment['Eye_Motion_Count'] == m]['Eye_Motion_Distance']))
                                
                        turn_speed_av = np.average(saccade_speeds)
                        turn_speed_sd = np.std(saccade_speeds)
                        
                        turn_duration_av = np.average(saccade_durations)
                        turn_duration_sd = np.std(saccade_durations)
                        
                        turn_distance_av = np.average(saccade_distances)
                        turn_distance_sd = np.std(saccade_distances)
                        
                        # overwrite results if bad calibration during turn.
                        if turn_duration_good_cal_no_gaps < turn_duration_no_gaps*time_threshold: # ignore results if the calibration was bad during turn.
                            turn_saccade_count = None
                            turn_speed_av = None
                            turn_speed_sd = None
                        
                            turn_duration_av = None
                            turn_duration_sd = None
                        
                            turn_distance_av = None
                            turn_distance_sd = None
                        
                        # head ang motion.
                        
                        turn_head_ang_av = np.average(df_segment[df_segment['Ang_Speed'].isna()==False]['Ang_Speed'])
                        
                        # split up average head ang movement into movement around x, y, and z axes.  
                        #x_ang_dis = df_segment[(df_segment['Motion_Type']=='up')|(df_segment['Motion_Type']=='down')]['Ang_Displacement'].sum()/turn_duration
                        #y_ang_dis = df_segment[(df_segment['Motion_Type']=='cw')|(df_segment['Motion_Type']=='ccw')]['Ang_Displacement'].sum()/turn_duration
                        #z_ang_dis = df_segment[(df_segment['Motion_Type']=='left')|(df_segment['Motion_Type']=='right')]['Ang_Displacement'].sum()/turn_duration
                        
                        
                        def ang_average(df):
                            if len(df)>0:
                                return np.average(df)
                            else:
                                return 0
                            
                        def ang_sd(df):
                            if len(df)>0:
                                return np.std(df)
                            else:
                                return 0
                            
                        # Find average head ang movement around x, y, and z axes. This is actually speed.
                        #x_ang_dis = ang_average(df_segment[(df_segment['Motion_Type']=='up')|(df_segment['Motion_Type']=='down')& (df_segment['Ang_Speed'].isna() == False)]['Ang_Speed'])
                        #z_ang_dis = ang_average(df_segment[(df_segment['Motion_Type']=='cw')|(df_segment['Motion_Type']=='ccw')& (df_segment['Ang_Speed'].isna() == False)]['Ang_Speed'])
                        #y_ang_dis = ang_average(df_segment[(df_segment['Motion_Type']=='left')|(df_segment['Motion_Type']=='right')& (df_segment['Ang_Speed'].isna() == False)]['Ang_Speed'])
                        
                        # split further
                        x_ang_speed_up = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='up') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        x_ang_speed_down = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='down') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())        
                        z_ang_speed_cw = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='cw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        z_ang_speed_ccw = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='ccw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())                    
                        y_ang_speed_left = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='left')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        y_ang_speed_right = ang_average(df_segment[(df_segment['Motion_Type_Clean']=='right')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        
                        x_ang_speed_up_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='up') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        x_ang_speed_down_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='down') & (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())        
                        z_ang_speed_cw_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='cw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        z_ang_speed_ccw_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='ccw')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())                    
                        y_ang_speed_left_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='left')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        y_ang_speed_right_sd = ang_sd(df_segment[(df_segment['Motion_Type_Clean']=='right')& (df_segment['Ang_Speed_Clean'].isna() == False)]['Ang_Speed_Clean'].unique())
                        
                        
                        # All durations divided by turn duration to get a duration fraction.
                        x_ang_dur_up = df_segment[(df_segment['Motion_Type_Clean']=='up')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        x_ang_dur_down = df_segment[(df_segment['Motion_Type_Clean']=='down')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        z_ang_dur_cw = df_segment[(df_segment['Motion_Type_Clean']=='cw')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        z_ang_dur_ccw = df_segment[(df_segment['Motion_Type_Clean']=='ccw')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        y_ang_dur_left = df_segment[(df_segment['Motion_Type_Clean']=='left')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        y_ang_dur_right = df_segment[(df_segment['Motion_Type_Clean']=='right')]['Ang_Duration_Clean'].unique().sum()/turn_duration
                        
                        
                        
                        no_motion_dur = 1-x_ang_dur_up-x_ang_dur_down-z_ang_dur_cw-z_ang_dur_ccw-y_ang_dur_left-y_ang_dur_right
                        
                        # Let's look at the dot products of the head motion.  Let's add the two values for a quantitative sense of head motion.
                        # No change means the dot products are 1.  We will subtract these from the total number of rows.  This value will be 0 if no head motion at all.
                        dot_sum = len(df_segment) - (df_segment['neg_z_dot'].sum() + df_segment['x_dot'].sum())/2
                        
                        # normalize by dividing by turn duration
                        dot_sum = dot_sum / turn_duration
                        
                        # we can also use the change in dot products to get a quantitative sense of the head acceleration
                        diff_array = abs(np.diff(df_segment['x_dot'])/np.diff(df_segment['Game_Time'])) + abs(np.diff(df_segment['neg_z_dot'])/np.diff(df_segment['Game_Time']))
                        
                        head_dot_acc = max(diff_array) - min(diff_array)
                        
                        # head range
                        
                        head_range_z = max(df_segment['neg_z_vector_rotated'].apply(lambda x: x[2])) - min(df_segment['neg_z_vector_rotated'].apply(lambda x: x[2]))
                        head_range_x = max(df_segment['x_vector_rotated'].apply(lambda x: x[0])) - min(df_segment['x_vector_rotated'].apply(lambda x: x[0]))
                        
                        head_range_z = head_range_z / turn_duration
                        head_range_x = head_range_x / turn_duration
                        
                        head_range_up_down = max(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1])) - min(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))
                        head_range_left_right = max(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0])) - min(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))
                        
                        head_range_up_down = head_range_up_down / turn_duration
                        head_range_left_right = head_range_left_right / turn_duration
                        
                        head_speed_up_down = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])) 
                        head_speed_left_right = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])) 
                        
                        head_acc_up_down = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                        head_acc_left_right = ang_average(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                        
                        head_sd_acc_up_down = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                        head_sd_acc_left_right = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])/np.diff(df_segment['Game_Time'])) 
                                            
                        head_sd_speed_up_down = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[1]))/np.diff(df_segment['Game_Time'])) 
                        head_sd_speed_left_right = ang_sd(np.diff(df_segment['neg_z_vector_rotated'].apply(lambda x: x[0]))/np.diff(df_segment['Game_Time'])) 
                        
                        
    
                        
                        # head lin motion.
                        #turn_head_speed_av = np.average(df_segment['Head_Speed'])
                        #turn_head_acc_av = np.average(df_segment['Head_Acc'])
                        
                        # split up average head lin movement into movement along x, y, and z axes.  All divided by turn duration to get a displacement "rate".
                        #x_dis = df_segment['Head_x_dis'].sum()/turn_duration
                        #y_dis = df_segment['Head_y_dis'].sum()/turn_duration
                        #z_dis = df_segment['Head_z_dis'].sum()/turn_duration                    
                        
                        # rates
                        turn_left_blink_rate = turn_left_blink_count / turn_duration_no_gaps # no_gaps to avoid the uncertainty about what happens in gaps.
                        turn_right_blink_rate = turn_right_blink_count / turn_duration_no_gaps
                        if turn_duration_good_cal_no_gaps < turn_duration_no_gaps*time_threshold:
                            fixation_rate = None
                        else:
                            fixation_rate = turn_fixation_count / turn_duration_good_cal_no_gaps # good_cal time since we don't count durations in bad calibration regions.
                        
                        # timers for stressor
                        turn_time_stressor_total = turn_time + turn_time_stressor
                        turn_time_stressor_total_no_gaps = turn_time_no_gaps + turn_time_stressor_no_gaps
                        
                        turn_time_stressor_total_good_cal = turn_time_good_cal + turn_time_stressor_good_cal
                        turn_time_stressor_total_good_cal_no_gaps = turn_time_good_cal_no_gaps + turn_time_stressor_good_cal_no_gaps
                    
                        
                        
                        variable_list = [j,l,turn_duration,turn_duration_no_gaps, turn_duration_good_cal,turn_duration_good_cal_no_gaps,\
                                                turn_time,turn_time_no_gaps, turn_time_good_cal, turn_time_good_cal_no_gaps,\
                                            turn_time_stressor_total,turn_time_stressor_total_no_gaps,turn_time_stressor_total_good_cal,turn_time_stressor_total_good_cal_no_gaps,\
                                                turn_left_blink_count,turn_right_blink_count,turn_fixation_count,\
                                            turn_left_blink_rate,turn_right_blink_rate,fixation_rate,\
                                            turn_fixation_av,turn_fixations_sd, \
                                                turn_saccade_count,turn_speed_av,turn_speed_sd,turn_distance_av,turn_distance_sd,turn_duration_av,turn_duration_sd,\
                                                turn_head_ang_av,x_ang_speed_up,x_ang_speed_down,z_ang_speed_cw,z_ang_speed_ccw,\
                                                y_ang_speed_left,y_ang_speed_right,\
                                                x_ang_speed_up_sd,x_ang_speed_down_sd,z_ang_speed_cw_sd,z_ang_speed_ccw_sd,\
                                                y_ang_speed_left_sd,y_ang_speed_right_sd,\
                                                x_ang_dur_up,x_ang_dur_down,z_ang_dur_cw,z_ang_dur_ccw,\
                                                y_ang_dur_left,y_ang_dur_right,no_motion_dur,dot_sum,head_range_z,head_range_x,\
                                                    head_range_up_down,head_range_left_right,\
                                                    head_dot_acc,\
                                        head_speed_up_down,head_speed_left_right,\
                                        head_sd_speed_up_down,head_sd_speed_left_right,\
                                        head_acc_up_down,head_acc_left_right,\
                                        head_sd_acc_up_down,head_sd_acc_left_right, UR_time,LR_time,UL_time,LL_time]
                        

                                
                        features_per_window.append(variable_list)
                                                
                                                #x_ang_dis,y_ang_dis,z_ang_dis,\
                                                #x_ang_dur,y_ang_dur,z_ang_dur,no_motion_dur])
                        
                        

                        
                # for stressor time, add time from previous games.      
                turn_time_stressor += max(df[(df['CurrentCondition']==j)]['Game_Time'])
                turn_time_stressor_no_gaps += max(df[(df['CurrentCondition']==j)]['Game_Time_No_Gaps'])
                turn_time_stressor_good_cal += max(df[(df['CurrentCondition']==j)]['Game_Time_Good_Cal'])
                turn_time_stressor_good_cal_no_gaps += max(df[(df['CurrentCondition']==j)]['Game_Time_Good_Cal_No_Gaps'])
    df_feature_window = pd.DataFrame(features_per_window, columns = ['Stressor','Player_Window_Count',\
                                                                'Turn_Duration','Turn_Duration_No_Gaps','Turn_Duration_Good_Cal','Turn_Duration_Good_Cal_No_Gaps', 'Turn_Time_in_Game (center of turn)',\
                                                                'Turn_Time_No_Gaps','Turn_Time_Good_Cal','Turn_Time_Good_Cal_No_Gaps','Turn_Time_Stressor','Turn_Time_Stressor_No_Gaps','Turn_Time_Stressor_Good_Cal','Turn_Time_Stressor_Good_Cal_No_Gaps','Left_Blinks','Right_Blinks',\
                                                                'Fixations', 'Left_Blink_Rate','Right_Blink_Rate',\
                                                                'Fixation_Rate','Fixation_Duration_Av','Fixation_Duration_SD',\
                                                                'Saccades','Saccades_Speed_Av','Saccades_Speed_SD','Saccades_Distance_Av','Saccades_Distance_SD','Saccades_Duration_Av','Saccades_Duration_SD',\
                                                                'Av_Ang_Speed',\
                                                                'av_speed_up','av_speed_down','av_speed_cw','av_speed_ccw','av_speed_left','av_speed_right',\
                                                                'sd_speed_up','sd_speed_down','sd_speed_cw','sd_speed_ccw','sd_speed_left','sd_speed_right',\
                                                                'dur_frac_up','dur_frac_down','dur_frac_cw','dur_frac_ccw','dur_frac_left','dur_frac_right',\
                                                                'no_motion_dur_frac','dot_sum','head_range_z','head_range_x',\
                                                                'head_range_up_down','head_range_left_right','head_dot_acc',\
                                                                'head_speed_up_down','head_speed_left_right',\
                                                                'head_sd_speed_up_down','head_sd_speed_left_right',\
                                                                'head_acc_up_down','head_acc_left_right',\
                                                                'head_sd_acc_up_down','head_sd_acc_left_right','UR_time','LR_time','UL_time','LL_time'])
                                                                #'x_angle_av_speed','y_angle_av_speed','z_angle_av_speed',\
                                                                #'x_angle_dur_frac','y_angle_dur_frac','z_angle_dur_frac','no_motion_dur_frac'])

    return df_feature_window


if __name__ == "__main__":
    import paho.mqtt.client as mqtt
    try:
        def onMessageData(mqttClient, userdata,msg):
            begin = time.time()
            # print(msg.payload.decode("utf-8"))
            try:
                dataOld = pd.read_csv('oldData.csv')
            except:
                dataOld = pd.DataFrame()
            payLoad = msg.payload.decode("utf-8")
            payLoad = json.loads(payLoad)

            data = pd.DataFrame(eval(payLoad[0]), index=[0])#expensive
            for i in range(1,len(payLoad)):
                data = pd.concat([data, pd.DataFrame(eval(payLoad[i]), index=[0])])
            
            if len(dataOld) < 10:
                try:
                    dataPrimer = pd.read_csv('dataPrimer.csv')
                    dataPrimer = pd.concat([dataPrimer,data])
                    dataPrimer.to_csv('dataPrimer.csv') 
                    #print(data.keys())
                except:
                    dataPrimer = data
                    dataPrimer.to_csv('dataPrimer.csv')
                if len(dataPrimer)> 85:
                    dataOld = cleanData2(dataPrimer)
                    dataOld.to_csv('oldData.csv')
                else:
                    pass
            else:                
                cleanedData = cleanData2(data)
                
                cleanedData = pd.concat([dataOld, cleanedData])
                cleanedData[-12:]
                output = inference(cleanedData[-12:],model)
                #print(cleanedData.keys())
                print(f"This is where the output woudl be {output}")
                mqttClient2 = mqtt.Client(f"Biometrics_{args.id}_Response")
                mqttClient2.connect(args.ip, 1883)
                mqttClient2.loop_start()
                info = mqttClient2.publish(
                    topic = f'Biometrics_{args.id}/inference',
                    payload = str(output).encode("utf-8"),
                    qos = 0
                )
                info.wait_for_publish()
                print(info.is_published())
                cleanedData[1:].to_csv('oldData.csv')
                print(time.time()-begin)
                mqttClient2.loop_stop()
        mqttClient = mqtt.Client(f"Biometrics_{args.id}_Server")
        mqttClient.message_callback_add(f'Biometrics_{args.id}/data', onMessageData)
        mqttClient.connect(args.ip, 1883)
        # start a new thread
        mqttClient.subscribe(f'Biometrics_{args.id}/data')
        
        mqttClient.loop_forever()



        recievedFile = datetime.utcnow().isoformat()

        

    except Exception as e:
        #print('Failed to process {} records. See the error below'.format( event['Records'][0]['dynamodb']['Keys']['timeStamp']['S']))
        info = mqttClient.publish(
                topic = f'Biometrics_{args.id}/inference',
                payload = str(e),
                qos = 0
            )
        print(str(e))
        info.wait_for_publish()
