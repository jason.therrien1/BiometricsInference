import time
import paho.mqtt.client as mqtt
import docker


def on_message_registration(mqttclient, userdata, msg):
    print('Received a new device ', str(msg.payload.decode('utf-8')))
    client = docker.from_env()
    spinUpContainer(client, str(msg.payload.decode('utf-8')))
    print("Spinning Up Container")


def spinDownContainer(client,containerName):
    containers = client.containers.list()
    for i in containers:
        if i.name == containerName:
            print(i.name, i.id)
            container = client.containers.get(i.id)
            container.stop()

def spinUpContainer(client, id):
#    client = docker.from_env()
    containers = client.containers.list(all)
    containerCheck = 0
    for i in containers:
        if i.name == f"Biometrics_Server_{id}":
            print(i.name, i.id)
            container = client.containers.get(i.id)
            containerCheck = 1
    if containerCheck == 0:
        client.containers.run("biometricsinference:latest", f"--id {id} --ip 192.168.1.16 --lstm 20", detach = True, name = f"Biometrics_Server_{id}")
    else:
        container.start()
                                                                                                                                        
mqttIPAddress = '192.168.1.16'#'18.216.145.62'

# Give a name to this MQTT client
mqttclient = mqtt.Client("Biometrics_LoadBalancer")
mqttclient.message_callback_add('Biometrics/Registration', on_message_registration)

# IP address of your MQTT broker, using ipconfig to look up it  
mqttclient.connect(mqttIPAddress, 1883)
# 'greenhouse/#' means subscribe all topic under greenhouse
mqttclient.subscribe('Biometrics/#')

mqttclient.loop_forever()
