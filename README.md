# BiometricsInference
I am using the Eclipse MQTT docker container for my MQTT client, it can be found here 

https://hub.docker.com/_/eclipse-mosquitto 

 

Set it up and have it running. I am not using any advanced networking with Docker. The container inherits my local IP address and messages can be sent/received on port 1883. Once the MQTT client is running there is a dashboard available at  

http://localhost:18083/#/ 

Documentation here  

https://docs.emqx.com/en/enterprise/v4.4/getting-started/dashboard-ee.html#introduction 

Now you should have MQTT up and running locally, but the magic leap wont run locally so you will need to determine the IP address for the computer you are running MQTT on, in my case that is 192.168.1.16 so you will need to change that to your IP address in any of the scripts.  

You will need to build the biometricsinference container*, and make sure you have Orchestrator.py running as a system process (it needs access to docker for the system).  

 

At the time of this release Orchestrator.py will spin up a new docker container, or run an existing container associated with the `Ml Mqtt ID` in the inspector for Biometrics in Unity 

 
![Semantic description of image](mlmqttid.png "Image Title")
 

(3rd from the bottom there). While I have set it to 05, it accepts a string. Minimal testing has been done with other strings, but it has no problems handling them.  

 

Whenever the Scenario is loaded it will first make an attempt to register with the Orchestrator, if everything is setup correctly the orchestrator should spin up a docker container that the Scenario can uniquely subscribe to based on the Ml Mqtt ID (well if you gave it a unique one.) 

 

Finally mqttExample.ipynb exists not just as an example of the ideas necessary to work with MQTT, but a few system integrations tests to get help you debug any situations (at least these are the ones I have been using, feel free to add more if you find them useful!). 

*as a note, I have included a zip file of the BiometricsModel, this is because when I initially pushed it to git and pulled it back down, TF couldn't parse the model. restoring it from a zip file worked though. I know there is some file level encoding funk that can happen with git. Till I figure out what the issue is, i am just including the model as a zip.a