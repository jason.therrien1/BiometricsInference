# Pull the base image with python 3.8 as a runtime for your Lambda
#FROM public.ecr.aws/lambda/python:3.8
FROM ubuntu:latest

# Install OS packages for Pillow-SIMD
RUN apt update
Run apt install python3 pip -y
RUN apt-get -y install libpq-dev gcc
# Copy the earlier created requirements.txt file to the container
COPY requirements.txt ./
COPY BiometricsModel ./
COPY test.json ./

# Install the python requirements from requirements.txt
RUN python3 -m pip install -r requirements.txt

# Replace Pillow with Pillow-SIMD to take advantage of AVX2

# Copy the earlier created app.py file to the container

COPY app.py ./


# Set the CMD to your handler
#CMD ["app.lambda_handler"]
ENTRYPOINT [ "python3", "app.py"]
